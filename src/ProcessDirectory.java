import java.io.File;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.synesis.config.Folders;
import com.synesis.dto.policy.EmployerDetailsDto;
import com.synesis.dto.policy.PolicyDetails;
//import com.synesis.model.EmpData;
import com.synesis.utility.RequestUtils;

public class ProcessDirectory {

    public static WebDriver driver;

    public static RequestUtils reqUtils = new RequestUtils();
	private static ObjectMapper mapper = new ObjectMapper();

    public static DateTimeFormatter pdtf = DateTimeFormatter.ofPattern("MM/dd/yy");

	public static void main(String[] args) {
		if ( args.length >= 1 ) {
			try {
				driver = setUpPhantomJS(Folders.WINDOWS_PHANTOMJS_EXECUTABLE_PATH, Folders.WINDOWS_PHANTOMJS_LOGFILE_PATH);

				processDirectory(new File(args[0]));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void processDirectory(File dir) {
		File[] detailsFiles = dir.listFiles();

		if (detailsFiles != null) {
			for (File detailsFile : detailsFiles) {
				if ( detailsFile.isDirectory() ) {
					processDirectory(detailsFile);
				} else {
					processFile(detailsFile);
				}
			}
		}
	}

	public static void processFile(File file) {
		try {
			if (file.getName().contains("details")) {
				driver.navigate().to(file.toURL());

				EmployerDetailsDto employerDetailsDto = getEmployerDetailsDto();
				postPolicyDetails(employerDetailsDto);
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}

	public static void postPolicyDetails(EmployerDetailsDto details) {
		try {
			String postData = "option=policies&details=" + URLEncoder.encode(mapper.writeValueAsString(details), "UTF-8");
			byte[] data = reqUtils.postURLwithoutProxy(Folders.serverURL, postData);
			String result = new String(data);
			System.out.println(result);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
//	public static String getFileContents(File file) {
//		StringBuffer sb = new StringBuffer();
//		try {
//			BufferedReader br = new BufferedReader(new FileReader(file));
//			String line;
//
//			while ((line = br.readLine()) != null) {
//				sb.append(line);
//			}
//			br.close();
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return sb.toString();
//	}


    public static EmployerDetailsDto getEmployerDetailsDto() {
        EmployerDetailsDto employerDetailsDto = new EmployerDetailsDto();
        List<PolicyDetails> policyDetailsList = new ArrayList<>();

        employerDetailsDto.setFileNumber(driver.findElement(By.id("ASPxRoundPanel1_dxlbInfoFile")).getText());
        employerDetailsDto.setFein(driver.findElement(By.id("ASPxRoundPanel1_dxlbInfoFEIN")).getText());
        employerDetailsDto.setCompanyName(driver.findElement(By.id("ASPxRoundPanel1_dxlbInfoCompanyName")).getText());
        employerDetailsDto.setPrimaryName(driver.findElement(By.id("ASPxRoundPanel1_dxlbInfoPrimaryName")).getText());
        employerDetailsDto.setAddress(driver.findElement(By.id("ASPxRoundPanel1_dxlbInfoAddress")).getText());
        employerDetailsDto.setCity(driver.findElement(By.id("ASPxRoundPanel1_dxlbInfoCity")).getText());
        employerDetailsDto.setState(driver.findElement(By.id("ASPxRoundPanel1_dxlbInfoState")).getText());

        WebElement element = driver.findElement(By.xpath("//*[@id='gridPolicyInfo_DXMainTable']/tbody"));

        List<WebElement> childTrs = element.findElements(By.xpath("./child::*"));

        for (int i = 1, k = 0; i < childTrs.size() - 1; i++, k++) {

            List<WebElement> childTds = childTrs.get(i).findElements(By.xpath("./child::*"));
            PolicyDetails policyDetails = new PolicyDetails();
    		policyDetails.setLineNumber(i);

            for (int j = 0; j < childTds.size(); j++) {

            		switch (j) {
                    	case 0:
                            policyDetails.setPolicyNumber(driver.findElement(By.id("gridPolicyInfo_cell" + k + "_0_lblPolNumberAdd_" + k)).getText());
                            break;
                        case 1:
                            policyDetails.setInsuranceCarrier(childTds.get(j).getText());
                            break;
                        case 2:
                            policyDetails.setNaic(childTds.get(j).getText());
                            break;
                        case 3:
                            policyDetails.setEffectiveDate(childTds.get(j).getText());
                            break;
                        case 4:
                            policyDetails.setExpirationDate(childTds.get(j).getText());
                            break;
                        case 5:
                            policyDetails.setCancelDate(childTds.get(j).getText());
                            break;

                    }
            }

            policyDetailsList.add(policyDetails);
        }
        employerDetailsDto.setPolicyDetails(policyDetailsList);
        return employerDetailsDto;

    }


    public static WebDriver setUpPhantomJS(String PHANTOMJS_EXECUTABLE_PATH, String PHANTOMJS_LOGFILE_PATH) throws Exception {
        DesiredCapabilities desiredCapabilities = DesiredCapabilities.phantomjs();
        desiredCapabilities.setJavascriptEnabled(true);
        desiredCapabilities.setCapability("loadImages", true);
        desiredCapabilities.setCapability("takesScreenshot", true);
        desiredCapabilities.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,PHANTOMJS_EXECUTABLE_PATH);
        desiredCapabilities.setCapability(PhantomJSDriverService.PHANTOMJS_PAGE_SETTINGS_PREFIX + "userAgent",
                "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.91 Safari/537.36");
        desiredCapabilities.setBrowserName("chrome");
        desiredCapabilities.setVersion("62");
        desiredCapabilities.setPlatform(Platform.WINDOWS);
        ArrayList<String> cliArgsCap = new ArrayList<String>();

//        /**
//         * Commented for Proxy Setup. Enable by passing proxy port in the address option with Luminati
//         */
//        cliArgsCap.add("--proxy=" + PROXY_ADDRESS + ":" + port);
//        cliArgsCap.add("--proxy-type=http");
        
        cliArgsCap.add("--webdriver-logfile=" + PHANTOMJS_LOGFILE_PATH + "phantomjsdriver.log");
        cliArgsCap.add("--webdriver-loglevel=NONE");

        desiredCapabilities.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, cliArgsCap);
        WebDriver driver = new PhantomJSDriver(desiredCapabilities);

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

        return driver;
    }
}
