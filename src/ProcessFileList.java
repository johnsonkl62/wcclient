import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.synesis.config.Folders;
import com.synesis.dto.policy.EmployerDetailsDto;
import com.synesis.dto.policy.PolicyDetails;
//import com.synesis.model.EmpData;
import com.synesis.utility.RequestUtils;

public class ProcessFileList {

    public static WebDriver driver;

    public static RequestUtils reqUtils = new RequestUtils();

    public static DateTimeFormatter pdtf = DateTimeFormatter.ofPattern("MM/dd/yy");

	public static String BASE_DIRECTORY = "D:/Synesis/ClassReviewedErrors/";

	public static void main(String[] args) {
		if ( args.length == 1 ) {
			try {
				driver = setUpPhantomJS(Folders.WINDOWS_PHANTOMJS_EXECUTABLE_PATH, Folders.WINDOWS_PHANTOMJS_LOGFILE_PATH);

				processListFile(args[0]);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void processListFile(String filename) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(filename));
			String line;

			while ((line = br.readLine()) != null) {
				try {
					processFile(line);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			br.close();
		} catch ( Exception e ) {
			System.out.println("ProcessListFile Exception");
			e.printStackTrace();
		}
		
	}
	
	public static void processFile(String filename) {
		try {
			driver.navigate().to(new File(filename).toURL());

			EmployerDetailsDto employerDetailsDto = getEmployerDetailsDto();
			
			String s = employerDetailsDto.getFileNumber() + "\t" + getEmployerInfo(employerDetailsDto.getPolicyDetails());

			log(s);

		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}


	private static String getEmployerInfo(List<PolicyDetails> policyHistory) {
		LocalDate ld = LocalDate.now();
		
		String carrier = "CANCELLED";
        if (policyHistory != null) {
        	for ( PolicyDetails policy : policyHistory ) {
	            try {
	            	String expDate = policy.getExpirationDate();
	            	String canDate = policy.getCancelDate();
	            	if (expDate != null && !"00/00/00".contentEquals(expDate) &&
	            			(canDate == null || canDate.trim().length() == 0 ) ) { 
	            		try {
	            			LocalDate cd = LocalDate.parse(expDate, pdtf);
	            			if (cd.isAfter(ld)) {
	            				carrier = policy.getExpirationDate() + "\t" + policy.getNaic() + "\t" + policy.getInsuranceCarrier();
	            				ld = cd;
	            			}
	            		} catch (Exception e) {
	            			e.printStackTrace();
	            		}
	            	}
	            } catch (Exception e) {
	                e.printStackTrace();
	                throw e;
	            }
        	}
        }
        return carrier;
	}

	public static EmployerDetailsDto getEmployerDetailsDto() {
        EmployerDetailsDto employerDetailsDto = new EmployerDetailsDto();
        List<PolicyDetails> policyDetailsList = new ArrayList<>();

        employerDetailsDto.setFileNumber(driver.findElement(By.id("ASPxRoundPanel1_dxlbInfoFile")).getText());
        employerDetailsDto.setFein(driver.findElement(By.id("ASPxRoundPanel1_dxlbInfoFEIN")).getText());
        employerDetailsDto.setCompanyName(driver.findElement(By.id("ASPxRoundPanel1_dxlbInfoCompanyName")).getText());
        employerDetailsDto.setPrimaryName(driver.findElement(By.id("ASPxRoundPanel1_dxlbInfoPrimaryName")).getText());
        employerDetailsDto.setAddress(driver.findElement(By.id("ASPxRoundPanel1_dxlbInfoAddress")).getText());
        employerDetailsDto.setCity(driver.findElement(By.id("ASPxRoundPanel1_dxlbInfoCity")).getText());
        employerDetailsDto.setState(driver.findElement(By.id("ASPxRoundPanel1_dxlbInfoState")).getText());

        WebElement element = driver.findElement(By.xpath("//*[@id='gridPolicyInfo_DXMainTable']/tbody"));

        List<WebElement> childTrs = element.findElements(By.xpath("./child::*"));

        for (int i = 1, k = 0; i < childTrs.size() - 1; i++, k++) {

            List<WebElement> childTds = childTrs.get(i).findElements(By.xpath("./child::*"));
            PolicyDetails policyDetails = new PolicyDetails();
    		policyDetails.setLineNumber(i);

            for (int j = 0; j < childTds.size(); j++) {

            		switch (j) {
                    	case 0:
                            policyDetails.setPolicyNumber(driver.findElement(By.id("gridPolicyInfo_cell" + k + "_0_lblPolNumberAdd_" + k)).getText());
                            break;
                        case 1:
                            policyDetails.setInsuranceCarrier(childTds.get(j).getText());
                            break;
                        case 2:
                            policyDetails.setNaic(childTds.get(j).getText());
                            break;
                        case 3:
                            policyDetails.setEffectiveDate(childTds.get(j).getText());
                            break;
                        case 4:
                            policyDetails.setExpirationDate(childTds.get(j).getText());
                            break;
                        case 5:
                            policyDetails.setCancelDate(childTds.get(j).getText());
                            break;

                    }
            }

            policyDetailsList.add(policyDetails);
        }
        employerDetailsDto.setPolicyDetails(policyDetailsList);
        return employerDetailsDto;

    }


    public static WebDriver setUpPhantomJS(String PHANTOMJS_EXECUTABLE_PATH, String PHANTOMJS_LOGFILE_PATH) throws Exception {
        DesiredCapabilities desiredCapabilities = DesiredCapabilities.phantomjs();
        desiredCapabilities.setJavascriptEnabled(true);
        desiredCapabilities.setCapability("loadImages", true);
        desiredCapabilities.setCapability("takesScreenshot", true);
        desiredCapabilities.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,PHANTOMJS_EXECUTABLE_PATH);
        desiredCapabilities.setCapability(PhantomJSDriverService.PHANTOMJS_PAGE_SETTINGS_PREFIX + "userAgent",
                "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.91 Safari/537.36");
        desiredCapabilities.setBrowserName("chrome");
        desiredCapabilities.setVersion("62");
        desiredCapabilities.setPlatform(Platform.WINDOWS);
        ArrayList<String> cliArgsCap = new ArrayList<String>();

//        /**
//         * Commented for Proxy Setup. Enable by passing proxy port in the address option with Luminati
//         */
//        cliArgsCap.add("--proxy=" + PROXY_ADDRESS + ":" + port);
//        cliArgsCap.add("--proxy-type=http");
        
        cliArgsCap.add("--webdriver-logfile=" + PHANTOMJS_LOGFILE_PATH + "phantomjsdriver.log");
        cliArgsCap.add("--webdriver-loglevel=NONE");

        desiredCapabilities.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, cliArgsCap);
        WebDriver driver = new PhantomJSDriver(desiredCapabilities);

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

        return driver;
    }


	private static void log(String s) {
		PrintWriter pw = null;
		try {
			System.out.println(s);
			String logFileName = BASE_DIRECTORY + "Corrections.txt";
			pw = new PrintWriter(new FileOutputStream(new File(logFileName), true));
			pw.println(s);
			pw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (pw != null) {
				pw.close();
			}
		}
	}
}
