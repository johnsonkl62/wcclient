package com.synesis.mirko;

import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Random;

public class Main {

	private static DateTimeFormatter pdtf = DateTimeFormatter.ofPattern("MM/dd/yy");
	public static Random r = new Random(Calendar.getInstance().getTimeInMillis());

//	  |  853830 | WOODLYNDE SCHOOL                             |
//	  | 3213051 | ABOVE & BEYOND                               |
//	  | 2458224 | SHARITZ BUS SERVICE INC                      |
//	  | 2666670 | GALLERY INTERIORS INC                        |
//	  | 2978746 | MATERIALS DELIVERY SERVICES INC              |
//	  | 3108478 | J FULLARD CONSTRUCTION                       |
//	  | 2136692 | DARROWS MOTOR COMPANY                        |
//	  | 2236142 | NIVERT METAL SUPPLY INC                      |
//	  | 2402884 | SOUTH HILLS SCHOOL OF BUSINESS & TECHNOLOGY  |
//	  | 2615321 | ASSOCIATED CONSTRUCTION & MANAGEMENT CORP    |
//	  | 3093711 | GAUMER INDUSTRIES                            |
//	  |  107824 | KENNEDY TRANSFER INC                         |
//	  | 2808704 | USMA CORP                                    |
//	  | 2994666 | TIMBER END INC                               |
//	  | 2997148 | G C WALL INC                                 |
//	  | 2806965 | QUALITY CLEANERS INC                         |
//	  | 3371767 | EMBASSY SUITES TEMPE                         |
//	  | 2891644 | SCI CAN INC                                  |
//	  | 2935489 | PENNSYLVANIA HOMETOWN STORE LLC              |
//	  | 2058430 | PETE DONATI & SONS INC                       |
//	  | 3081365 | WOODLAND LANDSCAPE CO                        |
//	  | 2807718 | DAVID BARD HOME REMODELING                   |
//	  | 2099025 | PAKULSKI INC                                 |
//	  | 2149173 | RAM ELECTRIC INC                             |
//	  | 3313669 | LM HOMES LLP                                 |
//	  | 3005785 | CARY TRANSPORTATION INC                      |
//	  | 2535944 | DALTON LUMBER & SUPPLY CO INC                |
//	  | 2620476 | CIRCLE MFG CO INC                            |
	
//    public static void main(String[] args) {
//    	try {
//			String name = "CARY TRANSPORTATION INC";
//			String num = "3005785";
//			
////        PCRB pcrb = new PCRB();
//			DLI pcrb = new DLI(24000);
//			
////        pcrb.loadInitialDetails();
//			pcrb.loadViewState();
//			Main.delaySecond(2,5);
//			
//			String url =  pcrb.getDetailsURL(name, num); 
//			Main.delaySecond(1,7);
//			
//			HashMap<String, Object> data = pcrb.getDetails(url,num);
//			printData(data);
//
////    	LocalFileUtils.getDetailsFromFile("");
//		} catch (SocketException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//    }
//    
//    public static void delaySecond(int min, int max) {
//    	try {
//			int delay =  min + ((max-min)*r.nextInt(max-min));
//			Thread.sleep(delay*1000);
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
//    }
//    
//
//	public static void printData(HashMap<String, Object> hm) {
//		try {
//			System.out.println("URL:  " + hm.get("url").toString());
//			@SuppressWarnings("unchecked")
//			HashMap<String, String> info = (HashMap<String, String>) hm.get("info");
//			System.out.println();
//			System.out.println("Info: ");
//			for (String is : info.keySet()) {
//				System.out.println("   " + is + "\t" + info.get(is));
//			}
//
//			@SuppressWarnings("unchecked")
//			HashMap<String, String>[] details = (HashMap<String, String>[]) hm.get("details");
//
//			System.out.println();
//			System.out.println("Details: ");
//
//			int index = 0;
//			LocalDate ld = LocalDate.parse(details[0].get("Policy Expiration Date"), pdtf);
//
//			for (int i = 1; i < details.length; i++) {
//				HashMap<String, String> detail = details[i];
//
//				LocalDate cd = LocalDate.parse(details[i].get("Policy Expiration Date"), pdtf);
//				if (cd.isAfter(ld)) {
//					index = i;
//				}
//				for (String key : detail.keySet()) {
//					System.out.print("   " + detail.get(key) + "\t");
//				}
//				System.out.println();
//			}
//			// String carrier = details[index].get("Insurance Carrier");
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

}
