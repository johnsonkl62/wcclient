package com.synesis.mirko;

import java.io.ByteArrayInputStream;
import java.io.PrintWriter;
import java.net.SocketException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.xpath.XPathConstants;

import org.w3c.dom.Document;

import com.sun.org.apache.xml.internal.dtm.ref.DTMNodeList;
import com.synesis.mirko.utils.DOMUtils;
import com.synesis.utility.RequestUtils;

public class PCRB extends Object {
//    private String viewState;
//    private String eventValidation;
//    private final String rootURL = "https://www.pcrbdata.com/PolicyCoverageInformation/";
//
//    public PCRB() {
//    }
//
//    public void loadInitialDetails() throws java.net.SocketException {
//        try {
//			byte[] data = RequestUtils.getURL(rootURL + "PANameSearch");
//			try {
//				PrintWriter pw = new PrintWriter("SearchFrame.html");
//				pw.println(new String(data));
//				pw.close();
//			} catch ( Exception e ) {
//				
//			}
//			Document doc = DOMUtils.getDocument(new ByteArrayInputStream(data));
//
//			this.viewState = (String) DOMUtils.xpathSearch(doc, "//input[@id='__VIEWSTATE']/@value", XPathConstants.STRING);
//			this.eventValidation = (String) DOMUtils.xpathSearch(doc, "//input[@id='__EVENTVALIDATION']/@value", XPathConstants.STRING);
//		} catch (SocketException e) {
//			throw e;
//		}
//    }
//
//    public String getDetailsURL(String name, String num){
//        try {
//            URLEncoder.encode(this.viewState, "UTF-8");
//            String postData = String.format("txtNameSearch=%s&__VIEWSTATE=%s&__EVENTVALIDATION=%s&btnSearch=Search",
//                    URLEncoder.encode(name, "UTF-8"),
//                    URLEncoder.encode(this.viewState, "UTF-8"),
//                    URLEncoder.encode(this.eventValidation, "UTF-8"));
//
//            byte[] data = RequestUtils.postURL(rootURL + "PANameSearch", postData);
//            try {
//            	PrintWriter pw = new PrintWriter("Summary-" + num + ".html");
//            	pw.println(new String(data));
//            	pw.close();
//            } catch ( Exception e ) {
//            	
//            }
//            Document doc = DOMUtils.getDocument(new ByteArrayInputStream(data));
//            String detailsURL = (String) DOMUtils.xpathSearch(doc, String.format("//b[contains(text(),'%s')]/../../..//a/@onclick", num), XPathConstants.STRING);
//            if(detailsURL==null) return null;
//            Pattern r = Pattern.compile("^location\\.href=\\'(.*)\\'$");
//            Matcher m = r.matcher(detailsURL);
//            m.find();
//            detailsURL = rootURL + m.group(1);
//            return detailsURL;
//        } catch (Exception e){
//            System.out.print(e.toString());
//            return null;
//        }
//    }
//
//
//    public HashMap<String, Object> getDetails(String url, String num) throws java.net.SocketException {
//        if(url==null)return null;
//        HashMap<String, Object> output = new HashMap<String, Object>();
//        output.put("url", url);
//        byte[] data = RequestUtils.getURL(url);
//
//        try {
//        	PrintWriter pw = new PrintWriter("Details-" + num + ".html");
//        	pw.println(new String(data));
//        	pw.close();
//        } catch ( Exception e ) {
//        }
//
//        Document doc = DOMUtils.getDocument(new ByteArrayInputStream(data));
//
//        HashMap<String, String> info = new HashMap<String, String>();
//        DTMNodeList fieldNameEl = (DTMNodeList) DOMUtils.xpathSearch(doc, "//table[@class='record_list']//th[@class='list_header']", XPathConstants.NODESET);
//
//        for (int i = 0; i < fieldNameEl.getLength(); i++) {
//            String fieldName = fieldNameEl.item(i).getFirstChild().getNodeValue();
//            String fieldValue = fieldNameEl.item(i).getNextSibling().getFirstChild().getFirstChild().getNodeValue();
//            info.put(fieldName.substring(0, fieldName.length() - 1), fieldValue);
//        }
//        output.put("info", info);
//
//        DTMNodeList columnNameEl = (DTMNodeList) DOMUtils.xpathSearch(doc, "//table[@id='GridPolicySearch']//tr[@class='records_header']/th", XPathConstants.NODESET);
//        String[] columns = new String[columnNameEl.getLength()];
//        for (int i = 0; i < columnNameEl.getLength(); i++) {
//            columns[i] = columnNameEl.item(i).getFirstChild().getFirstChild().getNodeValue();
//        }
//
//        DTMNodeList rowEl = (DTMNodeList) DOMUtils.xpathSearch(doc, "//table[@id='GridPolicySearch']//tr[not(@class)]", XPathConstants.NODESET);
//        HashMap<String, String>[] details = new HashMap[rowEl.getLength()];
//        for (int i = 0; i < rowEl.getLength(); i++) {
//            HashMap<String, String> item = new HashMap<String, String>();
//            DTMNodeList cellEl = (DTMNodeList) DOMUtils.xpathSearch(rowEl.item(i), "./td//b", XPathConstants.NODESET);
//            for (int n = 0; n < cellEl.getLength(); n++) {
//                item.put(columns[n], cellEl.item(n).getFirstChild().getNodeValue());
//            }
//            details[i] = item;
//        }
//
//        output.put("details", details);
//
//        return output;
//    }
//    
}