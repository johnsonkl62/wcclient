package com.synesis.mirko.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.xpath.XPathConstants;

import org.apache.commons.io.FileUtils;
import org.w3c.dom.Document;

import com.sun.org.apache.xml.internal.dtm.ref.DTMNodeList;

public class LocalFileUtils {
    private static final String rootURL = "https://www.pcrbdata.com/PolicyCoverageInformation/";

    public LocalFileUtils() {
    }

    
    private static ByteArrayInputStream getFileContents(String filename) {
    	try {
    		return new ByteArrayInputStream(FileUtils.readFileToByteArray(new File(filename)));
    	} catch ( Exception e ) {
    		return null;
    	}
    }
    
    
    public static String getDetailsURLFromFile(String name, String num){
        try {
            Document doc = DOMUtils.getDocument(getFileContents("Summary-" + num + ".html"));

//            String searchString = String.format("//tr/td//*[text()='2735943']/../", num);
            
            String detailsURL = (String) DOMUtils.xpathSearch(doc, String.format("//b[contains(text(),'%s')]/../../..//a/@onclick", num), XPathConstants.STRING);
//            String detailsURL = (String) DOMUtils.xpathSearch(doc, searchString, XPathConstants.STRING);
//            String detailsURL = (String) DOMUtils.xpathSearch(doc, "//tr/td[contains(text(),'%s')]/..//a/@onclick", XPathConstants.STRING);
            if (detailsURL == null || "".equals(detailsURL) ) return null;
            Pattern r = Pattern.compile("^location\\.href=\\'(.*)\\'$");
            Matcher m = r.matcher(detailsURL);
            m.find();
            detailsURL = rootURL + m.group(1);
            return detailsURL;
        } catch (Exception e){
            System.out.print(e.toString());
            return null;
        }
    }

    public static HashMap<String, Object> getDetailsFromFile(String filename){

    	HashMap<String, Object> output = new HashMap<String, Object>();
    	
        Document doc = DOMUtils.getDocument(getFileContents(filename));

        HashMap<String, String> info = new HashMap<String, String>();
        DTMNodeList fieldNameEl = (DTMNodeList) DOMUtils.xpathSearch(doc, "//table[@class='record_list']//th[@class='list_header']", XPathConstants.NODESET);

        for (int i = 0; i < fieldNameEl.getLength(); i++) {
            String fieldName = fieldNameEl.item(i).getFirstChild().getNodeValue();
            String fieldValue = fieldNameEl.item(i).getNextSibling().getFirstChild().getFirstChild().getNodeValue();
            info.put(fieldName.substring(0, fieldName.length() - 1), fieldValue);
        }

        output.put("info", info);

        DTMNodeList columnNameEl = (DTMNodeList) DOMUtils.xpathSearch(doc, "//table[@id='GridPolicySearch']//tr[@class='records_header']/th", XPathConstants.NODESET);
        String[] columns = new String[columnNameEl.getLength()];
        for (int i = 0; i < columnNameEl.getLength(); i++) {
            columns[i] = columnNameEl.item(i).getFirstChild().getFirstChild().getNodeValue();
        }

        DTMNodeList rowEl = (DTMNodeList) DOMUtils.xpathSearch(doc, "//table[@id='GridPolicySearch']//tr[not(@class)]", XPathConstants.NODESET);
        HashMap<String, String>[] details = new HashMap[rowEl.getLength()];
        for (int i = 0; i < rowEl.getLength(); i++) {
            HashMap<String, String> item = new HashMap<String, String>();
            DTMNodeList cellEl = (DTMNodeList) DOMUtils.xpathSearch(rowEl.item(i), "./td//b", XPathConstants.NODESET);
            for (int n = 0; n < cellEl.getLength(); n++) {
                item.put(columns[n], cellEl.item(n).getFirstChild().getNodeValue());
            }
            details[i] = item;
        }

        output.put("details", details);

        return output;
    }
    
}