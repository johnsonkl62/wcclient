package com.synesis.mirko.utils;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.tidy.Tidy;

import javax.xml.namespace.QName;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import java.io.InputStream;

public class DOMUtils {
    public static Object xpathSearch(Node tidyDOM, String expression, QName type){
        XPath xPath = XPathFactory.newInstance().newXPath();
        try {
            XPathExpression xPathExpression = xPath.compile(expression);
            Object o = xPathExpression.evaluate(tidyDOM, type); 
            return o;
        } catch (Exception e) {
            return null;
        }
    }

    public static Document getDocument(InputStream response){
        Tidy tidy = new Tidy();
        tidy.setQuiet(true);
        tidy.setShowWarnings(false);
        return tidy.parseDOM(response, null);
    }
}
