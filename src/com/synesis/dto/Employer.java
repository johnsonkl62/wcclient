package com.synesis.dto;

import java.sql.Timestamp;
import java.util.Date;

public class Employer {
		private String fn;
		private String fein;    //   NEW FIELD (FEDERAL EMPLOYER ID NUMBER)
		private String code;
		private String mm;
		private Date xdate;
		private String name = "ERROR";
		private String pname = "";    //  NEW FIELD (PRIMARY NAME)
		private String addr;
		private String city;
		private String state;
		private String zip;
		private String county;
		private String carrier;
		private Timestamp updated;

		public Employer() {
		}

		public String getFn() {
			return fn;
		}

		public void setFn(String fn) {
			this.fn = fn;
		}

		public String getFein() {
			return fein;
		}

		public void setFein(String fein) {
			this.fein = fein;
		}

		public String getPname() {
			return pname;
		}

		public void setPname(String pname) {
			this.pname = pname;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getMm() {
			return mm;
		}

		public void setMm(String mm) {
			this.mm = mm;
		}

		public Date getXdate() {
			return xdate;
		}

		public void setXdate(Date xdate) {
			this.xdate = xdate;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getAddr() {
			return addr;
		}

		public void setAddr(String addr) {
			this.addr = addr;
		}

		public String getCity() {
			return city;
		}

		public void setCity(String city) {
			this.city = city;
		}

		public String getState() {
			return state;
		}

		public void setState(String state) {
			this.state = state;
		}

		public String getZip() {
			return zip;
		}

		public void setZip(String zip) {
			this.zip = zip;
		}

		public String getCounty() {
			return county;
		}

		public void setCounty(String county) {
			this.county = county;
		}

		public String getCarrier() {
			return carrier;
		}

		public void setCarrier(String carrier) {
			this.carrier = carrier;
		}

		public Timestamp getUpdated() {
			return updated;
		}

		public void setUpdated(Timestamp updated) {
			this.updated = updated;
		}

		@Override
		public String toString() {
			return  fn + "\t" + 
					county + "\t" + 
					updated + "\t" + 
					carrier + "\t" + 
					name + "\t" + 
					addr + "\t" + 
					city + "\t" + 
					state + "\t" + 
					zip + "\t" + 
					xdate + "\t" + 
					code + "\t" + 
					mm;
		}


}
