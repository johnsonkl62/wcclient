package com.synesis.dto;

public class DelayResponse {
	private Integer delay;
	private String myIp;	//  This is the remote IP address of the client received by the server and handed back.

	public String getMyIp() {
		return myIp;
	}
	public void setMyIp(String myIp) {
		this.myIp = myIp;
	}
	
	public Integer getDelay() {
		return delay;
	}
	public void setDelay(Integer delay) {
		this.delay = delay;
	}
}
