package com.synesis.dto.policy;

import java.io.Serializable;

public class PolicyDetails implements Serializable {

    private String policyNumber;
    private Integer lineNumber;
    private String insuranceCarrier;
    private String naic;
    private String effectiveDate;
    private String expirationDate;
    private String cancelDate;

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public Integer getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(Integer lineNumber) {
		this.lineNumber = lineNumber;
	}

    public String getInsuranceCarrier() {
        return insuranceCarrier;
    }

    public void setInsuranceCarrier(String insuranceCarrier) {
        this.insuranceCarrier = insuranceCarrier;
    }

    public String getNaic() {
        return naic;
    }

    public void setNaic(String naic) {
        this.naic = naic;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(String cancelDate) {
        this.cancelDate = cancelDate;
    }

    @Override
    public String toString() {
        return "\n\t" + policyNumber + "\t" +
        		insuranceCarrier + "\t" +
                naic + "\t" +
                effectiveDate + "\t" +
                expirationDate + "\t" +
                cancelDate;
    }
}
