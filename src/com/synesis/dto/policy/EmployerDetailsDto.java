package com.synesis.dto.policy;

import java.io.Serializable;
import java.util.List;

public class EmployerDetailsDto implements Serializable {

    private String fileNumber;
    private String fein;
    private String companyName;
    private String primaryName;
    private String address;
    private String city;
    private String state;

    private List<PolicyDetails> policyDetails;

    public String getFileNumber() {
        return fileNumber;
    }

    public void setFileNumber(String fileNumber) {
        this.fileNumber = fileNumber;
    }

    public String getFein() {
        return fein;
    }

    public void setFein(String fein) {
        this.fein = fein;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getPrimaryName() {
        return primaryName;
    }

    public void setPrimaryName(String primaryName) {
        this.primaryName = primaryName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public List<PolicyDetails> getPolicyDetails() {
        return policyDetails;
    }

    public void setPolicyDetails(List<PolicyDetails> policyDetails) {
        this.policyDetails = policyDetails;
    }

    @Override
    public String toString() {
        return "EmployerDetailsDto{" +
                "fileNumber='" + fileNumber + '\'' +
                ", fein='" + fein + '\'' +
                ", companyName='" + companyName + '\'' +
                ", primaryName='" + primaryName + '\'' +
                ", address='" + address + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", policyDetails=" + policyDetails +
                '}';
    }
}
