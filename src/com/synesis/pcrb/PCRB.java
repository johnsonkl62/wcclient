package com.synesis.pcrb;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.xpath.XPathConstants;

import org.apache.commons.io.FileUtils;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;

import com.sun.org.apache.xml.internal.dtm.ref.DTMNodeList;

public class PCRB extends Object {
    private String viewState;
    private String eventValidation;
    private final String rootURL = "https://www.pcrbdata.com/PolicyCoverageInformation/";

    private RequestUtils ru = new RequestUtils();
    
    public PCRB() {
//        loadInitialDetails();
    }

    public HashMap<String, Object> search(String companyName, String fileNumber){
//		Client4.sleep(1, 8, Delay.SECONDS);
        String detailsURL = getDetailsURL(companyName, fileNumber);
//		Client4.sleep(1, 4, Delay.SECONDS);
        return getDetails(detailsURL, fileNumber);
    }

    public void loadInitialDetails(){
        byte[] data = ru.getURL(rootURL + "PANameSearch");

        try {
			FileUtils.writeByteArrayToFile(new File("pcrb-initial.html"), data);
		} catch (IOException e) {
			e.printStackTrace();
		}
        
        Document doc = DOMUtils.getDocument(new ByteArrayInputStream(data));

        this.viewState = (String) DOMUtils.xpathSearch(doc, "//input[@id='__VIEWSTATE']/@value", XPathConstants.STRING);
        this.eventValidation = (String) DOMUtils.xpathSearch(doc, "//input[@id='__EVENTVALIDATION']/@value", XPathConstants.STRING);
    }

    private String getDetailsURL(String name, String num){
        try {
            URLEncoder.encode(this.viewState, "UTF-8");
            String postData = String.format("txtNameSearch=%s&__VIEWSTATE=%s&__EVENTVALIDATION=%s&btnSearch=Search",
                    URLEncoder.encode(name, "UTF-8"),
                    URLEncoder.encode(this.viewState, "UTF-8"),
                    URLEncoder.encode(this.eventValidation, "UTF-8"));

            byte[] data = ru.postURL(rootURL + "PANameSearch", postData);
            try {
    			FileUtils.writeByteArrayToFile(new File("pcrb-summary-" + num + ".html"), data);
    		} catch (IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
            
            Document doc = DOMUtils.getDocument(new ByteArrayInputStream(data));

            String detailsURL = (String) DOMUtils.xpathSearch(doc, String.format("//b[contains(text(),'%s')]/../../..//a/@onclick", num), XPathConstants.STRING);
            if(detailsURL==null) return null;
            Pattern r = Pattern.compile("^location\\.href=\\'(.*)\\'$");
            Matcher m = r.matcher(detailsURL);
            m.find();
            detailsURL = rootURL + m.group(1);
            return detailsURL;
        } catch (Exception e){
            System.out.print(e.toString());
            return null;
        }
    }

    private HashMap<String, Object> getDetails(String url, String num) {
        if(url==null)return null;
        HashMap<String, Object> output = new HashMap<String, Object>();
        output.put("url", url);
        byte[] data = ru.getURL(url);

        try {
			FileUtils.writeByteArrayToFile(new File("pcrb-details-" + num + ".html"), data);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        Document doc = DOMUtils.getDocument(new ByteArrayInputStream(data));

        HashMap<String, String> info = new HashMap<String, String>();
        DTMNodeList fieldNameEl = (DTMNodeList) DOMUtils.xpathSearch(doc, "//table[@class='record_list']//th[@class='list_header']", XPathConstants.NODESET);

        for (int i = 0; i < fieldNameEl.getLength(); i++) {
            String fieldName = fieldNameEl.item(i).getFirstChild().getNodeValue();
            String fieldValue = fieldNameEl.item(i).getNextSibling().getFirstChild().getFirstChild().getNodeValue();
            info.put(fieldName.substring(0, fieldName.length() - 1), fieldValue);
        }

        output.put("info", info);

        DTMNodeList columnNameEl = (DTMNodeList) DOMUtils.xpathSearch(doc, "//table[@id='GridPolicySearch']//tr[@class='records_header']/th", XPathConstants.NODESET);
        String[] columns = new String[columnNameEl.getLength()];
        for (int i = 0; i < columnNameEl.getLength(); i++) {
            columns[i] = columnNameEl.item(i).getFirstChild().getFirstChild().getNodeValue();
        }

        DTMNodeList rowEl = (DTMNodeList) DOMUtils.xpathSearch(doc, "//table[@id='GridPolicySearch']//tr[not(@class)]", XPathConstants.NODESET);
        HashMap<String, String>[] details = new HashMap[rowEl.getLength()];
        for (int i = 0; i < rowEl.getLength(); i++) {
            HashMap<String, String> item = new HashMap<String, String>();
            DTMNodeList cellEl = (DTMNodeList) DOMUtils.xpathSearch(rowEl.item(i), "./td//b", XPathConstants.NODESET);
            for (int n = 0; n < cellEl.getLength(); n++) {
                item.put(columns[n], cellEl.item(n).getFirstChild().getNodeValue());
            }
            details[i] = item;
        }

        output.put("details", details);

        return output;
    }


    public static HashMap<String, Object> getDetailsFromFile(String filename) {
    	
    	HashMap<String, Object> output = null;
		try {
			ByteArrayInputStream bais = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File(filename)));

			output = new HashMap<String, Object>();

//        try {
//			FileUtils.writeByteArrayToFile(new File("pcrb-details-" + num + ".html"), data);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

			Document doc = DOMUtils.getDocument(bais);

			HashMap<String, String> info = new HashMap<String, String>();
			DTMNodeList fieldNameEl = (DTMNodeList) DOMUtils.xpathSearch(doc, "//table[@class='record_list']//th[@class='list_header']", XPathConstants.NODESET);

			for (int i = 0; i < fieldNameEl.getLength(); i++) {
			    String fieldName = fieldNameEl.item(i).getFirstChild().getNodeValue();
			    String fieldValue = fieldNameEl.item(i).getNextSibling().getFirstChild().getFirstChild().getNodeValue();
			    info.put(fieldName.substring(0, fieldName.length() - 1), fieldValue);
			}

			output.put("info", info);

			DTMNodeList columnNameEl = (DTMNodeList) DOMUtils.xpathSearch(doc, "//table[@id='GridPolicySearch']//tr[@class='records_header']/th", XPathConstants.NODESET);
			String[] columns = new String[columnNameEl.getLength()];
			for (int i = 0; i < columnNameEl.getLength(); i++) {
			    columns[i] = columnNameEl.item(i).getFirstChild().getFirstChild().getNodeValue();
			}

			DTMNodeList rowEl = (DTMNodeList) DOMUtils.xpathSearch(doc, "//table[@id='GridPolicySearch']//tr[not(@class)]", XPathConstants.NODESET);
			HashMap<String, String>[] details = new HashMap[rowEl.getLength()];
			for (int i = 0; i < rowEl.getLength(); i++) {
			    HashMap<String, String> item = new HashMap<String, String>();
			    DTMNodeList cellEl = (DTMNodeList) DOMUtils.xpathSearch(rowEl.item(i), "./td//b", XPathConstants.NODESET);
			    for (int n = 0; n < cellEl.getLength(); n++) {
			        item.put(columns[n], cellEl.item(n).getFirstChild().getNodeValue());
			    }
			    details[i] = item;
			}

			output.put("details", details);
		} catch (DOMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        return output;
    }

    public boolean have_good_super_proxy() {
        return ru.have_good_super_proxy();
    }

    public void switch_session_id() {
    	ru.switch_session_id();
    }

    public void update_client() {
    	ru.update_client();
    }

    public void close() {
    	ru.close();
    }


}