package com.synesis.pcrb;

import java.io.DataOutputStream;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.PasswordAuthentication;
import java.net.Proxy;
import java.net.URL;
import java.util.Random;

public class RequestUtils {
    public static final String username = "lum-customer-synesis-zone-residential";
    public static final String password = "14b54989c7df";
    public String login = "";
    public static final int port = 22225;
    public static final String user_agent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36";
    public final int max_failures = 3;
    public final int req_timeout = 60*1000;
    public String country;
    public int fail_count;
    public int n_req_for_exit_node;
    public Random rng = new Random();
    public String proxy_session_id = Integer.toString(rng.nextInt(Integer.MAX_VALUE));
    public String session_id = Integer.toString(rng.nextInt(Integer.MAX_VALUE));
    public String host;
    
    private String browser = null;

	private Proxy super_proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("customer-synesis-session-"+proxy_session_id+"-servercountry-us.zproxy.luminati.io", 22225));

    public RequestUtils() {
    	session_id = Integer.toString(rng.nextInt(Integer.MAX_VALUE));
        login = username + "-session-" + session_id;

        System.setProperty("http.proxyHost", "customer-synesis-session-"+proxy_session_id+"-servercountry-us.zproxy.luminati.io");
        System.setProperty("http.proxyPort", "22225");
        System.setProperty("http.proxyUser", login);
        System.setProperty("http.proxyPassword", password);

	    Authenticator authenticator = new Authenticator() {
	    	@Override
	        public PasswordAuthentication getPasswordAuthentication() {
//	            return (new PasswordAuthentication(username, password.toCharArray()));
	            return (new PasswordAuthentication(login, password.toCharArray()));
	        }
	    };
	    Authenticator.setDefault(authenticator);

	    int bIndex = rng.nextInt(100);
	    if ( bIndex < 60 ) {
	    	browser = "Chrome";
	    } else if ( bIndex < 71 ) {
	    	browser = "IE";
	    } else if ( bIndex < 83 ) {
	    	browser = "Firefox";
	    } else {
	    	browser = "Edge";
	    }
	    
    }

    private void setBrowserData(HttpURLConnection connection) {

    	if ( "Chrome".equals(browser) ) {
			connection.setRequestProperty("connection", "keep-alive");
			connection.setRequestProperty("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36");
			connection.setRequestProperty("upgrade-insecure-requests", "1");
			connection.setRequestProperty("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
			connection.setRequestProperty("dnt", "1");
			connection.setRequestProperty("accept-encoding", "gzip, deflate");
			connection.setRequestProperty("accept-language", "en-US,en;q=0.9");

    	} else if ( "IE".equals(browser) ) {
    		connection.setRequestProperty("accept", "text/html, application/xhtml+xml, image/jxr, */*");
    		connection.setRequestProperty("accept-language", "en-US,en;q=0.5");
    		connection.setRequestProperty("user-agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; Touch; TNJB; rv:11.0) like Gecko");
    		connection.setRequestProperty("accept-encoding", "gzip, deflate");
    		connection.setRequestProperty("host", "13.59.170.61");
    		connection.setRequestProperty("connection", "Keep-Alive");

    	} else if ( "Firefox".equals(browser) ) {
    		connection.setRequestProperty("host", "13.59.170.61");
    		connection.setRequestProperty("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0");
    		connection.setRequestProperty("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
    		connection.setRequestProperty("accept-language", "en-US,en;q=0.5");
    		connection.setRequestProperty("accept-encoding", "gzip, deflate");
    		connection.setRequestProperty("connection", "keep-alive");
    		connection.setRequestProperty("upgrade-insecure-requests", "1");

    	} else {
    		connection.setRequestProperty("accept", "text/html, application/xhtml+xml, image/jxr, */*");
    		connection.setRequestProperty("accept-language", "en-US,en;q=0.5");
    		connection.setRequestProperty("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 Edge/16.16299");
    		connection.setRequestProperty("accept-encoding", "gzip, deflate");
    		connection.setRequestProperty("host", "13.59.170.61");
    		connection.setRequestProperty("connection", "Keep-Alive");
    	}
    }
    
    
    public byte[] getURL(String requestURL){
        HttpURLConnection connection = null;
        try {
            URL url = new URL(requestURL);
            connection = (HttpURLConnection) url.openConnection(super_proxy);
//            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Referer", "http://www.dli.pa.gov");

//            setBrowserData(connection);
            
            connection.setUseCaches(false);
            connection.setInstanceFollowRedirects(true);
            connection.setDoOutput(true);
            return DataUtils.readFullyAsBytes(connection.getInputStream());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    public byte[] postURL(String requestURL, String urlParameters) {
        HttpURLConnection connection = null;
        try {
            URL url = new URL(requestURL);
            connection = (HttpURLConnection) url.openConnection(super_proxy);
//            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
//            setBrowserData(connection);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("Content-Length", Integer.toString(urlParameters.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");
            connection.setInstanceFollowRedirects(true);
            connection.setUseCaches(false);
            connection.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.close();
            return DataUtils.readFullyAsBytes(connection.getInputStream());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    public boolean have_good_super_proxy() {
        return super_proxy != null && fail_count < max_failures;
    }

    public void switch_session_id() {
        session_id = Integer.toString(rng.nextInt(Integer.MAX_VALUE));
        n_req_for_exit_node = 0;
//      super_proxy = new HttpHost(host, port);
        super_proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("customer-synesis-session-"+proxy_session_id+"-servercountry-us.zproxy.luminati.io", 22225));
        update_client();
    }

    public void update_client() {
        close();
        String login = username+(country!=null ? "-country-"+country : "") + "-session-" + session_id;

        System.setProperty("http.proxyHost", "customer-synesis-session-"+proxy_session_id+"-servercountry-us.zproxy.luminati.io");
        System.setProperty("http.proxyPort", "22225");
        System.setProperty("http.proxyUser", login);
        System.setProperty("http.proxyPassword", password);

	    Authenticator authenticator = new Authenticator() {
	    	@Override
	        public PasswordAuthentication getPasswordAuthentication() {
//	            return (new PasswordAuthentication(username, password.toCharArray()));
	            return (new PasswordAuthentication(login, password.toCharArray()));
	        }
	    };
	    Authenticator.setDefault(authenticator);
	    
	    super_proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("customer-synesis-session-"+proxy_session_id+"-servercountry-us.zproxy.luminati.io", 22225));
//
//        CredentialsProvider cred_provider = new BasicCredentialsProvider();
//        cred_provider.setCredentials(new AuthScope(super_proxy),
//                new UsernamePasswordCredentials(login, password));
//        RequestConfig config = RequestConfig.custom()
//                .setConnectTimeout(req_timeout)
//                .setConnectionRequestTimeout(req_timeout)
//                .build();
//        PoolingHttpClientConnectionManager conn_mgr =
//                new PoolingHttpClientConnectionManager();
//        conn_mgr.setDefaultMaxPerRoute(Integer.MAX_VALUE);
//        conn_mgr.setMaxTotal(Integer.MAX_VALUE);
//        client = HttpClients.custom()
//                .setConnectionManager(conn_mgr)
//                .setProxy(super_proxy)
//                .setDefaultCredentialsProvider(cred_provider)
//                .setDefaultRequestConfig(config)
//                .build();
    }

    
    public void close() {
//        if (client != null)
//            try { client.close(); } catch (IOException e) {}
//        client = null;
    }
}
