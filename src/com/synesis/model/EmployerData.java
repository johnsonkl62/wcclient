package com.synesis.model;

import java.sql.Date;

public class EmployerData {

	private String code;
	private String mm;
	private Date xdate;

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMm() {
		return mm;
	}
	public void setMm(String mm) {
		this.mm = mm;
	}
	public Date getXdate() {
		return xdate;
	}
	public void setXdate(Date xdate) {
		this.xdate = xdate;
	}

	public String toString() {
		return "Code: " + code + 
               "  MM: " + mm + 
               "  Xdate: " + xdate;
	}
}
