package com.synesis.model;

public class EmployerBase {

	private String address;
	private String city;
	private String state;
	private String zip;
	private String county;

	public EmployerBase() {
	}
	
	public EmployerBase(String a, String c, String s, String z, String y) {
		address = a;
		city = c;
		state = s;
		zip = z;
		county = y;
	}
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	
	public String getCounty() {
		return county;
	}
	public void setCounty(String county) {
		this.county = county;
	}

	public String toString() {
		return 	" " + address +  
				", " + city + 
				", " + state +  
				"  " + zip +  
				"  [" + county + "]";

	}
	
}
