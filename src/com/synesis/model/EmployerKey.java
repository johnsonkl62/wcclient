package com.synesis.model;

public class EmployerKey {

	private String name = "";
	private String county = "";
	private Integer fn = -1;
	
	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Integer getFn() {
		return fn;
	}
	
	public void setFn(Integer fn) {
		this.fn = fn;
	}
	
	public String toString() {
		return "" + county + "\t" + fn + "\t" + name;
	}
}
