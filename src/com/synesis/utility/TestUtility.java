package com.synesis.utility;

import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

public class TestUtility {

	private static Random r = new Random();

	static private String[] createNamePermutations(String name) {
		ArrayList<String> cnp = new ArrayList<String>();

		String nn = name.
			replaceAll(" INC$","").
			replaceAll(" CO$", "").
			replaceAll(" LLP$","").
			replaceAll(" LLC$","").
			replaceAll(" LP$","").
			replaceAll(" CORP$","").
			replaceAll(" LTD$","").
			replaceAll(" PC$","").
			replaceAll(" D/B/A","").
	
			replaceAll(" INC[ \\.]","").
			replaceAll(" CO[ \\.]", "").
			replaceAll(" LLP[ \\.]","").
			replaceAll(" LLC[ \\.]","").
			replaceAll(" LP[ \\.]","").
			replaceAll(" CORP[ \\.]","").
			replaceAll(" LTD[ \\.]","").
			replaceAll(" PC[ \\.]","").
			replaceAll(" DBA ","").
			replaceAll("L/C/F","").
			replaceAll("D/B/A","");


//		Pattern p1 = Pattern.compile("");
//		
//		
//		//		BLAKLEY P A	--> P A BLAKLEY
//		//      BUCK PEARL S FOUNDATION INC -->   PEARL S BUCK FOUNDATION INC
//		//		JANO,RENEE   -->  RENEE JANO
//		//      W K SMITH CONSTRUCTION INC  -->   WK SMITH CONSTRUCTION INC
//		//      KUMP,WILLIAM,A  -->   WILLIAM A KUMP
//		StringBuffer sb = new StringBuffer();
//
//		String[] wordTerms = nn.split(" ");
//		switch (wordTerms.length) {
//		case 1:
//			//  no spaces
//			String[] commaTerms = nn.split(",");
//			for ( String ct : commaTerms ) {
//				if ( sb.length() > 0 ) sb.append(" ");
//				sb.append(ct);
//			}
//			cnp.add(sb.toString());
//			break;
//
//		case 2:
//			//  reverse terms
//			cnp.add(wordTerms[1] + " " + wordTerms[0]);
//			break;
//		
//		case 3:
//			break;
//		
//		case 4:
//			break;
//
//		default:
//			if (wordTerms[0].length() == 1 && 
//					wordTerms[1].length() == 1 &&
//					wordTerms[2].length() == 1 &&
//					wordTerms[1].equals("&")) {
//
//				sb.append(wordTerms[0] + wordTerms[1] + wordTerms[2]);
//				for ( int i = 3; i < wordTerms.length; i++ ) {
//					sb.append(" " + wordTerms[i]);
//				}
//				cnp.add(sb.toString());
//			}
//				
//		}


		if ( nn.matches("^([A-Z|a-z]) & ([A-Z|a-z]) (.*)$")) {
			//  H & H SETTLEMENT SERVICES INC
			StringBuffer s1 = new StringBuffer();
			
			String[] wordTerms = nn.split("\\s");
			s1.append(wordTerms[0] + wordTerms[1] + wordTerms[2]);
			for ( int i = 3; i < wordTerms.length; i++ ) {
				s1.append(" " + wordTerms[i]);
			}
			cnp.add(s1.toString());

		} else if ( nn.matches("^([A-Z|a-z])\\s([A-Z|a-z]) (.*)$") ) {
			// V W INTERNATIONAL
			StringBuffer s1 = new StringBuffer();

			String[] wordTerms = nn.split("\\s");
			s1.append(wordTerms[0] + wordTerms[1]);
			for ( int i = 2; i < wordTerms.length; i++ ) {
				s1.append(" " + wordTerms[i]);
			}
			cnp.add(s1.toString());

		} else if ( nn.matches("^([^\\s]*)\\s([^\\s])\\s&\\s([^\\s]*)\\s([^\\s])\\s(.*)$") ) {

			String[] terms = nn.split(" ");
//			for ( int i = 0; i < terms.length; i++ ) {
//				System.out.println(i + "\t" + terms[i]);
//			}
			cnp.add(terms[5] + " " + terms[0] + " " + terms[1] + " & " + terms[3] + " " + terms[4]);
		
		} else if ( nn.matches("^([^\\s]*)\\s([^\\s]*)\\s([^\\s])\\s(.*)$") ) {
			//  BUCK PEARL S FOUNDATION INC
			String[] terms = nn.split(" ");
			cnp.add(terms[1] + " " + terms[2] + " " + terms[0] + nn.substring(terms[0].length() + terms[1].length() + terms[2].length() + 2));

		} else if ( nn.matches("^([^\\s]*)\\s([^\\s]*)\\s([^\\s])$") ) {
			//  AMMONS JOHN L INC
			String[] terms = nn.split(" ");
			cnp.add(terms[1] + " " + terms[2] + " " + terms[0] + nn.substring(terms[0].length() + terms[1].length() + terms[2].length() + 2));

		} else if ( nn.matches("^([A-Z|a-z]*),([A-Z|a-z]*)$") ) {

			String[] terms = nn.split(",");
			cnp.add(terms[1] + " " + terms[0]);

		} else if ( nn.matches("^([A-Z|a-z]*),([A-Z|a-z]*),([A-Z|a-z]*)$") ) {
			// ROCK,ROBERT,C
			String[] terms = nn.split(",");
			cnp.add(terms[1] + " " + terms[2] + " " + terms[0]);

		} else {
			String[] terms = nn.split(" ");
			if ( terms.length >= 2 ) {
				cnp.add(terms[0] + " " + terms[1]);
				cnp.add(terms[1] + " " + terms[0]);
			}
		}
		return cnp.toArray(new String[0]);
	}

	
	private static Proxy proxy = null;

    public static byte[] getURL(String requestURL, int port) throws java.net.SocketException {
        HttpURLConnection connection = null;
        try {
        	proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("207.244.242.208", port));
        	
            URL url = new URL(requestURL);
            connection = (HttpURLConnection) url.openConnection(proxy);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Referer", "https://www.pcrbdata.com/PolicyCoverageInformation/PANameSearch");
            connection.setUseCaches(false);
            connection.setInstanceFollowRedirects(true);
            connection.setDoOutput(true);
            return DataUtils.readFullyAsBytes(connection.getInputStream());
        } catch (java.net.SocketException jnse ) {
        	throw jnse;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    public static String getRequest(String url, int port) throws java.net.SocketException {
    	return new String(getURL(url,port));
    }

//	String[] testNames = {
//	"AMMONS JOHN L INC",
//	"ROCK,ROBERT,C",
//	"V W INTERNATIONAL",
//	"BUCK PEARL S FOUNDATION INC",
//	"H & H SETTLEMENT SERVICES LLC",
//	"CHARLES N & ANN D MARSHALL",
//	"HAAG MARY J & NANCY L KIEFFER GENERAL PARTNERSHIP"};
//
//for (String name : testNames) {
//System.out.println(name);
//String[] s = createNamePermutations(name);
//
//for ( String s1 : s) {
//	System.out.println("  " + s1);
//}
//System.out.println();
//}
//

	public static void main(String[] args) {
//		for ( int port = 24000; port < 24010; port++ ) {
//			try {
//				System.out.println("Trying proxy port " + port);
//				String s = getRequest("https://www.pcrbdata.com/PolicyCoverageInformation/PANameSearch", port);
//				System.out.println(s);
//			} catch (SocketException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//
//		try {
//		    String line;
//		    Process p = Runtime.getRuntime().exec("tasklist /FI \"IMAGENAME eq phantomjs.exe\"");
////		    	    (System.getenv("windir") +"\\system32\\"+"tasklist.exe");
//		    BufferedReader input =
//		            new BufferedReader(new InputStreamReader(p.getInputStream()));
//		    while ((line = input.readLine()) != null) {
//		        System.out.println(line);
//		    }
//		    input.close();
//		} catch (Exception err) {
//		    err.printStackTrace();
//		}
	
		ArrayList<String> searchTerms = new ArrayList<String>();
		searchTerms.add("A");
		while (searchTerms.size() > 0) {
			String st = searchTerms.remove(0);
			int results = search(st);
			if ( results > 5000 ) {
				searchTerms.addAll(split(st));
			} else {
				//  Process results
			}
		}

		System.out.println(System.getProperty("os.name"));
		
	}

	private static Collection<? extends String> split(String st) {
		//  A -->   A[A-M], A[N-Z], A[^A-Z]
		//  A[A-M] --> A[A-
		return null;
	}

	private static int search(String st) {
		return 3000 + r.nextInt(4000);
	}

}
