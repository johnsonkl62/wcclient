package com.synesis.utility;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

/**
 *
 * @author 
 */
public class HTTPUtils {
   
    public static String sendPostRequest(String url, Map<String,String> params, Map<String,String> headers, ResponseHandler reqHandler)
    {
        HttpPost post = new HttpPost(url);
        String resp = null;
        try
        {
            for(Map.Entry<String, String> itr:params.entrySet()){
                post.setEntity(new StringEntity(itr.getKey()+"="+itr.getValue(), "UTF-8"));
            }
            for(Map.Entry<String, String> itr:headers.entrySet()){
                post.setHeader(itr.getKey(), itr.getValue());
            }

            HttpClient httpClient = new DefaultHttpClient();
            resp = (String)httpClient.execute(post, reqHandler);
        }
        catch(UnsupportedEncodingException e1) {
        	System.out.println("Unsupported Encoding:");
//        	e1.printStackTrace();
        }
        catch(ClientProtocolException e) { 
        	System.out.println("ChromeClient Protocol:");
//        	e.printStackTrace();
        }
        catch(IOException ioE) {
        	System.out.println("IO Exception:");
//        	ioE.printStackTrace();
        }
        catch(Exception e2) {
        	System.out.println("Exception:");
        	e2.printStackTrace();
        }
        return resp;
    }

}
