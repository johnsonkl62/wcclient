package com.synesis.utility;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileReader;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

import javax.xml.xpath.XPathConstants;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;

import com.sun.org.apache.xml.internal.dtm.ref.DTMNodeList;
import com.synesis.mirko.utils.DOMUtils;

public class TestDetailsFile {

	public static DateTimeFormatter pdtf = DateTimeFormatter.ofPattern("MM/dd/yy");
	static private DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	public static void main(String[] args) {

		try {
			BufferedReader br = new BufferedReader(new FileReader(args[0]));
			StringBuffer sb = new StringBuffer();
			String line;

			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
			br.close();

		
			HashMap<String, Object> results = extractDetails("Test", sb.toString());

			String html = formatInfo(results);

			// Post result / get new delay
			String carrier = extractCarrier(results);

			System.out.println("Carrier = " + carrier);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	
	public static HashMap<String, Object> extractDetails(String url, String data) {
        if (data == null ) 
        	return null;

        HashMap<String, Object> output = new HashMap<String, Object>();
        output.put("url", url);

        Document doc = DOMUtils.getDocument(new ByteArrayInputStream(data.getBytes()));

        HashMap<String, String> info = new HashMap<String, String>();
        DTMNodeList fieldNameEl = (DTMNodeList) DOMUtils.xpathSearch(doc, "//table[@class='record_list']//th[@class='list_header']", XPathConstants.NODESET);

        for (int i = 0; i < fieldNameEl.getLength(); i++) {
			String fieldName = null;
			String fieldValue = null;
        	try {
				fieldName = fieldNameEl.item(i).getFirstChild().getNodeValue();
				fieldValue = fieldNameEl.item(i).getNextSibling().getFirstChild().getFirstChild().getNodeValue();
				info.put(fieldName.substring(0, fieldName.length() - 1), fieldValue);
			} catch (DOMException e) {
				System.out.println("DOMException:  Could not extract field value for " + fieldName);
			}
        }

        output.put("info", info);

        DTMNodeList columnNameEl = (DTMNodeList) DOMUtils.xpathSearch(doc, "//table[@id='GridPolicySearch']//tr[@class='records_header']/th", XPathConstants.NODESET);
        String[] columns = new String[columnNameEl.getLength()];
        for (int i = 0; i < columnNameEl.getLength(); i++) {
            columns[i] = columnNameEl.item(i).getFirstChild().getFirstChild().getNodeValue();
        }

        DTMNodeList rowEl = (DTMNodeList) DOMUtils.xpathSearch(doc, "//table[@id='GridPolicySearch']//tr[not(@class)]", XPathConstants.NODESET);
        HashMap<String, String>[] details = new HashMap[rowEl.getLength()];
        for (int i = 0; i < rowEl.getLength(); i++) {
            HashMap<String, String> item = new HashMap<String, String>();
            DTMNodeList cellEl = (DTMNodeList) DOMUtils.xpathSearch(rowEl.item(i), "./td//b", XPathConstants.NODESET);
            for (int n = 0; n < cellEl.getLength(); n++) {
                item.put(columns[n], cellEl.item(n).getFirstChild().getNodeValue());
            }
            details[i] = item;
        }

        output.put("details", details);

        return output;
 	}


	private static String formatInfo(HashMap<String,Object> pcrbInfo) {

		StringBuffer html = new StringBuffer();

		if (pcrbInfo.size() == 0 ) {
			return "<html><body><h2>Temporary Placeholder</h2></body></html>";
		}
		
		if (pcrbInfo != null) {
			try {
				// System.out.println("URL: " + hm.get("url").toString());

				@SuppressWarnings("unchecked")
				HashMap<String, String> info = (HashMap<String, String>) pcrbInfo.get("info");

				html.append("<h2>Policy Coverage (as of " + (LocalDateTime.now().format(dtf)) + ")</h2>");
				html.append("<table>");
				for (String is : info.keySet()) {
					html.append("<tr><td>" + is + "</td><td>" + (info.get(is) != null ? info.get(is) : "") + "</td></tr>");
				}
				html.append("</table>");
				html.append("<br/>");

				@SuppressWarnings("unchecked")
				HashMap<String, String>[] details = (HashMap<String, String>[]) pcrbInfo.get("details");

				String PN = "Policy Number";
				String IC = "Insurance Carrier";
				String NAIC = "NAIC";
				String PED = "Policy Effective Date";
				String PAD = "Policy Anniversary Date";
				String PXD = "Policy Expiration Date";
				String PCD = "Policy Cancellation Date";

				html.append("<table><tr><th>" + PN + "</th><th>" + IC + "</th><th>" + NAIC + "</th><th>" + PED
						+ "</th><th>" + PAD + "</th><th>" + PXD + "</th><th>" + PCD + "</th></tr>");

				for (HashMap<String, String> detail : details) {
					html.append("<tr>");
					html.append("<td>" + (detail.get(PN) != null ? detail.get(PN) : "") + "</td>");
					html.append("<td>" + (detail.get(IC) != null ? detail.get(IC) : "") + "</td>");
					html.append("<td>" + (detail.get(NAIC) != null ? detail.get(NAIC) : "") + "</td>");
					html.append("<td>" + (detail.get(PED) != null ? detail.get(PED) : "") + "</td>");
					html.append("<td>" + (detail.get(PAD) != null ? detail.get(PAD) : "") + "</td>");
					html.append("<td>" + (detail.get(PXD) != null ? detail.get(PXD) : "") + "</td>");
					html.append("<td>" + (detail.get(PCD) != null ? detail.get(PCD) : "") + "</td>");
					html.append("</tr>");
				}
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
		}
		return html.toString();
	}

	public static String extractCarrier(HashMap<String,Object> pcrbInfo) {
		String carrier = "*** Not found ***";
		if ( pcrbInfo != null ) {
			try {			 
				@SuppressWarnings("unchecked")
				HashMap<String,String>[] details = (HashMap<String,String>[])pcrbInfo.get("details");

				int index = -1;
				LocalDate ld = LocalDate.now();

				for ( int i = 0; i < details.length; i++ ) {
					String ped = details[i].get("Policy Expiration Date");
					String pcd = details[i].get("Policy Cancellation Date");
					if ( ! "00/00/00".equals(ped) && pcd == null) {
						try {
							LocalDate cd = LocalDate.parse(ped, pdtf);
							if ( cd.isAfter(ld) ) {
								index = i;
								ld = cd;
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
				if ( index >= 0 ) {
					carrier = trim(details[index].get("Insurance Carrier"));
				} else { // if ( maxIndex >= 0 ) {
					carrier = "CANCELLED";
				}
			} catch ( Exception e ) {
				e.printStackTrace();
			}
		}
		return carrier;
	}

	public static String trim(String s) {
		return ( s == null ? null : s.trim());
	}
	
}
