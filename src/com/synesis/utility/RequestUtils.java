package com.synesis.utility;

import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.Proxy.ProxyType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.synesis.config.Folders;
import com.synesis.dto.policy.EmployerDetailsDto;
import com.synesis.dto.policy.PolicyDetails;
import com.synesis.mirko.utils.DataUtils;
import com.synesis.model.Employer;

public class RequestUtils {

    public WebDriver driver;

	private int port = 24001;
	private Random r = new Random(Calendar.getInstance().getTimeInMillis());

//	private Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("127.0.0.1", port));
	private Proxy proxy = null;

    private ObjectMapper mapper = new ObjectMapper();

	private static final String rootURL = "https://www.pcrbdata.com/PolicyCoverageInformation/PANameSearch";
	private static final String DLI_REFERER = "https://www.dli.pa.gov/Businesses/Compensation/WC/insurance/Pages/Workers-Compensation-Insurance-Search-Form.aspx";
	
	private static final String PROXY_ADDRESS = "207.244.242.208";
	private static final String PROXY_RESET = "http://207.244.242.208:22999/api/refresh_sessions/";
//    private static final String PROXY_ADDRESS = "192.168.50.7";
//	private static final String PROXY_RESET = "http://127.0.0.1:22999/api/refresh_sessions/";

	private String PHANTOMJS_EXECUTABLE_PATH = "";
	private String PHANTOMJS_LOGFILE_PATH = "";
    
	public void initialize(int p, String exePath, String logFilePath) throws Exception {
		try {
			proxy = null;
			port = p;
//			proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("127.0.0.1", port));
			proxy = new Proxy();
			proxy.setProxyType(ProxyType.MANUAL);
			proxy.setHttpProxy(PROXY_ADDRESS + ":" + port);

			PHANTOMJS_EXECUTABLE_PATH = exePath;
			PHANTOMJS_LOGFILE_PATH = logFilePath;

			driver = setUpPhantomJS(exePath, logFilePath);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}
	
    public Employer getEmployer() throws com.fasterxml.jackson.core.JsonProcessingException {
    	resetProxySession();
		try {
			String employerString = getRequestWithoutProxy(Folders.serverURL + "?option=employer");
			return mapper.readValue(employerString, Employer.class);
		} catch (SocketException e) {
			e.printStackTrace();
			return null;
		}
    }

    public String getRequestWithoutProxy(String url) throws java.net.SocketException {
    	return new String(getURLwithoutProxy(url));
    }

    public byte[] getURLwithoutProxy(String requestURL) throws java.net.SocketException {
        HttpURLConnection connection = null;
        try {
            URL url = new URL(requestURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setUseCaches(false);
            connection.setRequestProperty("Referer", rootURL);
            connection.setInstanceFollowRedirects(true);
            connection.setDoOutput(true);
            return DataUtils.readFullyAsBytes(connection.getInputStream());
        } catch (java.net.SocketException jnse ) {
        	throw jnse;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    
    public void resetProxySession() {
		HttpURLConnection connection = null;
		try {
			URL url = new URL(PROXY_RESET + this.port);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setUseCaches(false);
			connection.setRequestProperty("Referer", rootURL);
			connection.setInstanceFollowRedirects(true);
			connection.setDoOutput(true);
			byte[] bytes = DataUtils.readFullyAsBytes(connection.getInputStream());
			System.out.println("ProxyReset: " + new String(bytes));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
    }
    
    
//    public byte[] postURL(String requestURL, String urlParameters) throws java.net.SocketException {
//        HttpURLConnection connection = null;
//        try {
//            URL url = new URL(requestURL);
//            connection = (HttpURLConnection) url.openConnection(proxy);
//            connection.setRequestMethod("POST");
//            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
//            connection.setRequestProperty("Content-Length", Integer.toString(urlParameters.getBytes().length));
//            connection.setRequestProperty("Content-Language", "en-US");
//            connection.setRequestProperty("Referer", rootURL);
//            connection.setInstanceFollowRedirects(true);
//            connection.setUseCaches(false);
//            connection.setDoOutput(true);
//            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
//            wr.writeBytes(urlParameters);
//            wr.close();
//            return DataUtils.readFullyAsBytes(connection.getInputStream());
//        } catch (java.net.SocketException jnse ) {
//        	throw jnse;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        } finally {
//            if (connection != null) {
//                connection.disconnect();
//            }
//        }
//    }


    public byte[] postURLwithoutProxy(String requestURL, String urlParameters) throws java.net.SocketException {
        HttpURLConnection connection = null;
        try {
            URL url = new URL(requestURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("Content-Length", Integer.toString(urlParameters.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");
            connection.setRequestProperty("Referer", rootURL);
            connection.setInstanceFollowRedirects(true);
            connection.setUseCaches(false);
            connection.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.close();
            return DataUtils.readFullyAsBytes(connection.getInputStream());
        } catch (java.net.SocketException jnse ) {
        	throw jnse;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    public String getSearchPage() {
        driver.navigate().to(rootURL);
        waitFor(By.id("dxrpSearch_dxtbNameSearch_I"), false, true, driver);
        return driver.getPageSource();
    }

    public static void waitFor(By by, boolean clickable, boolean visible, WebDriver driver) {
        WebDriverWait waiting2 = new WebDriverWait(driver, 120);

        if (clickable) {
            waiting2.until(ExpectedConditions.elementToBeClickable(by));
        } else if (visible) {
            waiting2.until(ExpectedConditions.visibilityOfElementLocated(by));
        }

    }

    public EmployerDetailsDto getEmployerDetailsDto() {
        EmployerDetailsDto employerDetailsDto = new EmployerDetailsDto();
        try {
			List<PolicyDetails> policyDetailsList = new ArrayList<>();
			employerDetailsDto.setFileNumber(driver.findElement(By.id("ASPxRoundPanel1_dxlbInfoFile")).getText());
			employerDetailsDto.setFein(driver.findElement(By.id("ASPxRoundPanel1_dxlbInfoFEIN")).getText());
			employerDetailsDto.setCompanyName(driver.findElement(By.id("ASPxRoundPanel1_dxlbInfoCompanyName")).getText());
			employerDetailsDto.setPrimaryName(driver.findElement(By.id("ASPxRoundPanel1_dxlbInfoPrimaryName")).getText());
			employerDetailsDto.setAddress(driver.findElement(By.id("ASPxRoundPanel1_dxlbInfoAddress")).getText());
			employerDetailsDto.setCity(driver.findElement(By.id("ASPxRoundPanel1_dxlbInfoCity")).getText());
			employerDetailsDto.setState(driver.findElement(By.id("ASPxRoundPanel1_dxlbInfoState")).getText());

			WebElement element = driver.findElement(By.xpath("//*[@id='gridPolicyInfo_DXMainTable']/tbody"));

			List<WebElement> childTrs = element.findElements(By.xpath("./child::*"));

			for (int i = 1, k = 0; i < childTrs.size() - 1; i++, k++) {

			    List<WebElement> childTds = childTrs.get(i).findElements(By.xpath("./child::*"));
			    PolicyDetails policyDetails = new PolicyDetails();
				policyDetails.setLineNumber(i);

			    for (int j = 0; j < childTds.size(); j++) {

			    		switch (j) {
			            	case 0:
			                    policyDetails.setPolicyNumber(driver.findElement(By.id("gridPolicyInfo_cell" + k + "_0_lblPolNumberAdd_" + k)).getText());
			                    break;
			                case 1:
			                    policyDetails.setInsuranceCarrier(childTds.get(j).getText());
			                    break;
			                case 2:
			                    policyDetails.setNaic(childTds.get(j).getText());
			                    break;
			                case 3:
			                    policyDetails.setEffectiveDate(childTds.get(j).getText());
			                    break;
			                case 4:
			                    policyDetails.setExpirationDate(childTds.get(j).getText());
			                    break;
			                case 5:
			                    policyDetails.setCancelDate(childTds.get(j).getText());
			                    break;
			            }
			    }
			    policyDetailsList.add(policyDetails);
			}

			employerDetailsDto.setPolicyDetails(policyDetailsList);
		} catch (Exception e) {
			e.printStackTrace();
			employerDetailsDto = null;
		}
        return employerDetailsDto;
    }


    public static void sleep(int secs) {
        try {
//            System.out.println("Sleeping for "+secs + " secs");
            Thread.sleep(secs*1000);
        } catch (Exception e) {
        }
    }

    
    public WebDriver setUpPhantomJS(String PHANTOMJS_EXECUTABLE_PATH, String PHANTOMJS_LOGFILE_PATH) throws Exception {
        DesiredCapabilities desiredCapabilities = DesiredCapabilities.phantomjs();
        desiredCapabilities.setJavascriptEnabled(true);
        desiredCapabilities.setCapability("loadImages", true);
        desiredCapabilities.setCapability("takesScreenshot", true);
        desiredCapabilities.setCapability(PhantomJSDriverService.PHANTOMJS_PAGE_CUSTOMHEADERS_PREFIX + "Referer", DLI_REFERER);
        desiredCapabilities.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,PHANTOMJS_EXECUTABLE_PATH);
        desiredCapabilities.setCapability(PhantomJSDriverService.PHANTOMJS_PAGE_SETTINGS_PREFIX + "userAgent",
                "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.91 Safari/537.36");
        desiredCapabilities.setBrowserName("chrome");
        desiredCapabilities.setVersion("112.0.5615.138");
        desiredCapabilities.setPlatform(Platform.WINDOWS);
        ArrayList<String> cliArgsCap = new ArrayList<String>();

//        /**
//         * Commented for Proxy Setup. Enable by passing proxy port in the address option with Luminati
//         */
//        cliArgsCap.add("--proxy=" + PROXY_ADDRESS + ":" + port);
//        cliArgsCap.add("--proxy-type=http");
        
        cliArgsCap.add("--webdriver-logfile=" + PHANTOMJS_LOGFILE_PATH + "phantomjsdriver-" + port + ".log");
        cliArgsCap.add("--webdriver-loglevel=NONE");

        desiredCapabilities.setCapability("proxy", proxy);
        desiredCapabilities.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, cliArgsCap);
        WebDriver driver = new PhantomJSDriver(desiredCapabilities);

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

        return driver;
    }

	public String getSummaryPage(String employerName) {
		try {
String name = employerName.replaceAll("'", "\\\\'");
			sleep(1 + r.nextInt(4));
			((JavascriptExecutor) driver).executeScript(
					"document.getElementById('dxrpSearch_dxtbNameSearch_I').value='" + name + "';");

			sleep(2 + r.nextInt(3));
			((JavascriptExecutor) driver)
					.executeScript("document.getElementsByName('dxrpSearch$dxbtnSearch')[0].click();");

        	waitFor(By.id("grid_DXMainTable"), false, true, driver);

			try {
				driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			System.out.print(e.toString());
			return null;
		}
		return driver.getPageSource();
	}    
    
    public String getDetailsPage(String employerFN) {
        try {
			sleep(2 + r.nextInt(3));
        	List<WebElement> we = driver.findElements(By.xpath(String.format("//td[contains(text(),'%s')]/../td/a", employerFN)));
        	if ( we.size() > 0 ) {
            	we.get(0).click();
                waitFor(By.id("gridPolicyInfo_DXMainTable"), false, true, driver);
        	} else {
        		return null;
        	}

        } catch (Exception e){
            System.out.print(e.toString());
            return null;
        }
        return driver.getPageSource();
    }

    public void resetPhantomJS() {
    	driver.quit();
    	driver = null;
    	try {
			setUpPhantomJS(PHANTOMJS_EXECUTABLE_PATH, PHANTOMJS_LOGFILE_PATH);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
}
