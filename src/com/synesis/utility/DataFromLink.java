/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.synesis.utility;

/**
 *
 * @author
 */
public class DataFromLink {

//	private static final String detailUrl = "http://146.145.230.82/PennsylvaniaPolicyCoverageInformation/";
//	private static final String url = "http://146.145.230.82/PennsylvaniaPolicyCoverageInformation/procedure_Name_Search_input.jsp";
//	
//	private static Long ninetyDays = new Long( 90 /*days*/ * 24 /*hours*/ * 60 /*minutes*/ * 60 /*seconds*/ * 1000 /*milliseconds*/ );
////	private static Long ageLimit = System.currentTimeMillis() - ninetyDays;
//
//	private static int reqHeaderSelector = 1;
//	
//	public static int getReqHeaderSelector() {
//		return reqHeaderSelector;
//	}
//
//	public static void setReqHeaderSelector(int req) {
//		reqHeaderSelector = req;
//	}
//
//	/**
//	 * @param args
//	 *            the command line arguments
//	 */
////	public static void main(String[] args) {
////		System.out.println(querySite(args[1]));
////	}
//
////	private static final String USER_AGENT = "Mozilla/5.0";
//	private static CloseableHttpClient httpclient;
//	private static BasicCookieStore cookieStore = new BasicCookieStore();
//	
//	static {
//		httpclient = HttpClients.custom()
//	            .setDefaultCookieStore(cookieStore)
//	            .build();
//	}
//
//	
//	
//	public static String queryFile(String filename) {
//    	try {
//    		//  Check if file exists
//    		File file = new File(filename);
//    		if (file.exists()) {
//    			try {
//    			BasicFileAttributes attr = Files.readAttributes(file.toPath(), BasicFileAttributes.class);
//    			FileTime fileTime = attr.creationTime();
//
//    			LocalDateTime ldt = LocalDateTime.now().minusDays(90);
//    			long before = ldt.toEpochSecond(ZoneOffset.UTC)*1000;  //  5 hours off, but who cares?
//
////    			System.out.println("File time: " + fileTime.toMillis());
////    			System.out.println("Age limit: " + ageLimit);
////    			System.out.println(fileTime.compareTo(FileTime.fromMillis(ageLimit)));
////
//    		    //minus 90 days
//    		    if ( fileTime.compareTo(FileTime.fromMillis(before)) <= 0 ) {
//        			//  Check date, delete if more than 3 months old
//        			Files.delete(file.toPath());
//        			return null;
//    		    }
//    			} catch ( Exception e1 ) {
//    				e1.printStackTrace();
//    			}
//    		}
//    		
//    		String line = null;
//    		BufferedReader br = new BufferedReader(new FileReader(filename));
//    		StringBuffer sb = new StringBuffer();
//    		while ((line = br.readLine()) != null) {
//    			sb.append(line);
//    		}
//    		br.close();
//    		return sb.toString();
//    	} catch ( Exception e ) {
////    		System.out.println(filename + " not found.");
//    	}
//    	return null;
//	}
//
//
//	public static String newQuerySite(String employerName) throws PCRBException {
//		String html = "";
//	
//		try {   	
//			List<NameValuePair> urlParameters;
//
//			HttpPost post = new HttpPost(url);
//
//			//  Change 2016-11-28
//			// add header
////			post.setHeader("User-Agent", USER_AGENT);  -- replaced below
//			if ( reqHeaderSelector % 4 == 1 ) {
//				//  Microsoft EDGE
//				Client.logOnly("newQuerySite:  MICROSOFT EDGE");
//				post.setHeader("Accept", "text/html, application/xhtml+xml, image/jxr, */*");
//				post.setHeader("Accept-language", "en-US,en;q=0.5");
//				post.setHeader("Accept-encoding", "gzip, deflate");
//				post.setHeader("Connection", "Keep-Alive");
//				post.setHeader("Host", "146.145.230.82");
//				post.setHeader("Referer", "http://146.145.230.82/PennsylvaniaPolicyCoverageInformation/procedure_Name_Search_input.jsp");
//				post.setHeader("User-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.79 Safari/537.36 Edge/14.14393");
//
//			} else if ( reqHeaderSelector % 4 == 3 ) {
//				//  Microsoft IE
//				Client.logOnly("newQuerySite:  MICROSOFT IE");
//				post.setHeader("Accept", "text/html, application/xhtml+xml, image/jxr, */*");
//				post.setHeader("Accept-language", "en-US,en;q=0.5");
//				post.setHeader("Accept-encoding", "gzip, deflate");
//				post.setHeader("Connection", "Keep-Alive");
//				post.setHeader("Host", "146.145.230.82");
//				post.setHeader("Referer", "http://146.145.230.82/PennsylvaniaPolicyCoverageInformation/procedure_Name_Search_input.jsp");
//				post.setHeader("User-agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; Touch; TNJB; rv:11.0) like Gecko");
//
//			} else if ( reqHeaderSelector % 4 == 2 ) {
//				//  Google Chrome
//				Client.logOnly("newQuerySite:  GOOGLE CHROME");
//				post.setHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
//				post.setHeader("Accept-encoding", "gzip, deflate, sdch");
//				post.setHeader("Accept-language", "en-US,en;q=0.8");
//				post.setHeader("Dnt","1");
//				post.setHeader("Connection", "keep-alive");
//				post.setHeader("Host", "146.145.230.82");
//				post.setHeader("Referer", "http://146.145.230.82/PennsylvaniaPolicyCoverageInformation/procedure_Name_Search_input.jsp");
//				post.setHeader("Upgrade-insecure-requests","1");
//				post.setHeader("User-agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
//
//			} else {
//				//  Firefox
//				Client.logOnly("newQuerySite:  MOZILLA FIREFOX");
//				post.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0");
//				post.setHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
//				post.setHeader("Accept-Encoding", "gzip, deflate");
//				post.setHeader("Accept-Language", "en-US,en;q=0.5");
//				post.setHeader("Connection", "keep-alive");
//				post.setHeader("Host", "146.145.230.82");
//				post.setHeader("Referer", "http://146.145.230.82/PennsylvaniaPolicyCoverageInformation/procedure_Name_Search_input.jsp");
//				post.setHeader("Upgrade-Insecure-Requests", "1");
//			}
//			//	End change 2016-11-28		
//			
//			urlParameters = new ArrayList<NameValuePair>();
//			urlParameters.add(new BasicNameValuePair("insuredname", employerName));
//			urlParameters.add(new BasicNameValuePair("parenthose", "true"));
//
////			ChromeClient.logOnly("newQuerySite:  insuredname=" + employerName);
//
//			post.setEntity(new UrlEncodedFormEntity(urlParameters));
//			
//			CloseableHttpResponse response = null;
//			try {
////				ChromeClient.logOnly("newQuerySite:  Executing post");
//				response = httpclient.execute(post);
////				ChromeClient.logOnly("newQuerySite:  Post completed");
//
////				for ( Cookie c : cookieStore.getCookies() ) {
////					System.out.println("Name: \"" + c.getName() + "\"   Value: \"" + c.getValue() + "\"");
////				}
//
////				ChromeClient.logOnly("newQuerySite:  Cookie store size=" + cookieStore.getCookies().size());
//
//				if ( cookieStore.getCookies().size() == 0 ) {
////					System.out.println("No cookies found");
//
//					Header[] headers=response.getHeaders("Set-Cookie");
//					if ( headers != null && headers.length > 0 ) {
//						String cookieString = headers[0].getValue();
//						String[] cookies = cookieString.split("; ");
//
//						for ( String cstr : cookies ) {
//							String[] nv = cstr.split("=");
//							if ( nv != null && nv.length > 1 ) {
//								BasicClientCookie cookie = new BasicClientCookie(nv[0], nv[1]);
//								cookieStore.addCookie(cookie);
//							}
//						}
//					}
//				}
//
//				html = getHTML(response);
//    			html = html.replaceAll("\t", "");
////				ChromeClient.logOnly("newQuerySite:  HTML retrieved");
//			} catch (Exception e) {
//				e.printStackTrace(Client.logFile);
//				e.printStackTrace();
//				throw new PCRBException();
//			} finally {
//				response.close();
//			}
//		} catch ( ClientProtocolException cpe1 ) {
//			cpe1.printStackTrace(Client.logFile);
//			cpe1.printStackTrace();
//		} catch ( IOException ioe ) {
//			ioe.printStackTrace(Client.logFile);
//			ioe.printStackTrace();
//		} catch ( Exception e1 ) {
//			e1.printStackTrace(Client.logFile);
//			e1.printStackTrace();
//			throw e1;
//		}
//		return html;
//	}
//
//	
//	public static String newQueryDetails(String link) {
//		String html = "";
//		try {
//			HttpGet request1 = new HttpGet(link);
//			CloseableHttpResponse response1 = null;
//
//
//			if ( reqHeaderSelector % 4 == 1 ) {
//				//  Microsoft EDGE
//				request1.setHeader("Accept", "text/html, application/xhtml+xml, image/jxr, */*");
//				request1.setHeader("Accept-language", "en-US,en;q=0.5");
//				request1.setHeader("Accept-encoding", "gzip, deflate");
//				request1.setHeader("Connection", "Keep-Alive");
//				request1.setHeader("Host", "146.145.230.82");
//				request1.setHeader("Referer", "http://146.145.230.82/PennsylvaniaPolicyCoverageInformation/procedure_Name_Search_input.jsp");
//				request1.setHeader("User-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.79 Safari/537.36 Edge/14.14393");
//
//			} else if ( reqHeaderSelector % 4 == 3 ) {
//				//  Microsoft IE
//				request1.setHeader("Accept", "text/html, application/xhtml+xml, image/jxr, */*");
//				request1.setHeader("Accept-language", "en-US,en;q=0.5");
//				request1.setHeader("Accept-encoding", "gzip, deflate");
//				request1.setHeader("Connection", "Keep-Alive");
//				request1.setHeader("Host", "146.145.230.82");
//				request1.setHeader("Referer", "http://146.145.230.82/PennsylvaniaPolicyCoverageInformation/procedure_Name_Search_input.jsp");
//				request1.setHeader("User-agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; Touch; TNJB; rv:11.0) like Gecko");
//
//			} else if ( reqHeaderSelector % 4 == 2 ) {
//				//  Google Chrome
//				request1.setHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
//				request1.setHeader("Accept-encoding", "gzip, deflate, sdch");
//				request1.setHeader("Accept-language", "en-US,en;q=0.8");
//				request1.setHeader("Dnt","1");
//				request1.setHeader("Connection", "keep-alive");
//				request1.setHeader("Host", "146.145.230.82");
//				request1.setHeader("Referer", "http://146.145.230.82/PennsylvaniaPolicyCoverageInformation/procedure_Name_Search_input.jsp");
//				request1.setHeader("Upgrade-insecure-requests","1");
//				request1.setHeader("User-agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
//
//			} else {
//				//  Firefox
//				request1.setHeader("Host", "146.145.230.82");
//				request1.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0");
//				request1.setHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
//				request1.setHeader("Accept-Language", "en-US,en;q=0.5");
//				request1.setHeader("Accept-Encoding", "gzip, deflate");
//				request1.setHeader("Referer", "http://146.145.230.82/PennsylvaniaPolicyCoverageInformation/procedure_Name_Search_input.jsp");
//				request1.setHeader("Connection", "keep-alive");
//				request1.setHeader("Upgrade-Insecure-Requests", "1");
//			}
//
//			try {
////				ChromeClient.logOnly("newQueryDetails:  Executing post");
//				response1 = httpclient.execute(request1);
////				ChromeClient.logOnly("newQueryDetails:  Post complete");
//				html = getHTML(response1);
//				html = html.replaceAll("\t", "");
//				html = html.replaceAll("<td class=\"record_alt\">", "<td class=\"record\">");
////				ChromeClient.logOnly("newQueryDetails:  HTML retrieved");
//				// dump(html, "html/" + fileNumber + "_detail.html");
//			} catch (Exception e) {
//				e.printStackTrace(Client.logFile);
//				e.printStackTrace();
//			} finally {
//				response1.close();
//			}
//		} catch (Exception e1) {
//			e1.printStackTrace(Client.logFile);
//			e1.printStackTrace();
//		}
//		return html;
//	}
//	
//	
//	//
//	// Extract the HTML source into a String
//	//
//	private static String getHTML(HttpResponse response)
//	{
//		BufferedReader rd;
//		StringBuffer result = new StringBuffer();
//		
//		try {
//			rd = new BufferedReader(
//			        new InputStreamReader(response.getEntity().getContent()));
//		
//			String line = "";
//			while ((line = rd.readLine()) != null) {
//				result.append(line);
//			}
//		} catch (UnsupportedOperationException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		
//		return (result.toString());
//	}
//
//	
//	
//	public static String querySite(String name){
//		String encName = name;
//		try {
//	        encName = URLEncoder.encode(name, "UTF-8");
//		} catch ( UnsupportedEncodingException uee ) {
//			encName = name;
//		}
//			
//        Map<String,String> params =new HashMap<String,String>();
//        Map<String,String> headers =new HashMap<String,String>();
//        headers.put("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
//        headers.put("Content-type","application/x-www-form-urlencoded");
//        headers.put("Accept-Language","en-US,en;q=0.5");
//        headers.put("Referer",url);
//        params.put("insuredname",encName);
//        params.put("parenthose","true");
//        
//        String resp=HTTPUtils.sendPostRequest( url, params, headers, new HTTPResponseHandler());
//        return resp;
//    }
//
//	public static String queryDetails(String linkString) {
//		Map<String, String> params = new HashMap<String, String>();
//		Map<String, String> headers = new HashMap<String, String>();
//		headers.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
//		headers.put("Content-type", "application/x-www-form-urlencoded");
//		headers.put("Accept-Language", "en-US,en;q=0.5");
//		headers.put("Referer", url);
//
//		String resp = HTTPUtils.sendPostRequest(detailUrl + linkString, params,
//				headers, new HTTPResponseHandler());
//		return resp;
//	}
}
