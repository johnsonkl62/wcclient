package com.synesis.luminati;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.mail.internet.MimeMultipart;

import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.openqa.selenium.TimeoutException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.synesis.client.pcrb.DelayResponse;
import com.synesis.config.Folders;
import com.synesis.dto.policy.EmployerDetailsDto;
import com.synesis.dto.policy.PolicyDetails;
import com.synesis.model.Employer;
import com.synesis.utility.RequestUtils;


public class LuminatiClient extends Thread {

    private static DateTimeFormatter pdtf = DateTimeFormatter.ofPattern("MM/dd/yy");
	private static DateTimeFormatter fdtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
	private static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	private static SimpleDateFormat dtsdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private ObjectMapper mapper = new ObjectMapper();

	private Employer EMP_ERROR = new Employer();
	
	public Integer detailsPageCount = 0;
	public final Integer detailsPageLimit = 100;

	public String firstFN = null;
	public String lastFN = null;

	private Random r = new Random(Calendar.getInstance().getTimeInMillis());

	private RequestUtils reqUtils = new RequestUtils(); 
	private int proxyPort = -1;
	
    private static final String WEBSITE_IP = "207.244.242.208";   //  Contabo server
//    private static final String WEBSITE_IP = "192.168.50.7";   //  Contabo server


    public void setProxyPort(int port) throws Exception {
		proxyPort = port;

        try {
            String myIp = Executor.newInstance()
                    .execute(Request.Get("https://api64.ipify.org/"))
                    .returnContent().asString();

            if (!WEBSITE_IP.equals(myIp)) {
                Folders.BASE_DIRECTORY = Folders.WINDOWS_BASE_DIRECTORY;  
                Folders.COLLECTION_DIRECTORY = Folders.WINDOWS_COLLECTION_DIRECTORY;  
                Folders.PHANTOMJS_EXECUTABLE_PATH = Folders.WINDOWS_PHANTOMJS_EXECUTABLE_PATH;
                Folders.PHANTOMJS_LOGFILE_PATH = Folders.WINDOWS_PHANTOMJS_LOGFILE_PATH;
            }
    		reqUtils.initialize(port, Folders.PHANTOMJS_EXECUTABLE_PATH, Folders.PHANTOMJS_LOGFILE_PATH);
    		
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
	}

	public void run() {

		try {
			//  Get delay from WCC
			String delayJson = reqUtils.getRequestWithoutProxy(Folders.serverURL + "?option=delay");

			DelayResponse delayResponse = mapper.readValue(delayJson, DelayResponse.class);

			int timeouts = 0;
			int errors = 0;

			// Scraping sequence ...
			while (delayResponse != null) {
				try {
					LocalDateTime pst = LocalDateTime.now();
					pst = pst.plusSeconds(delayResponse.getDelay());

					//  Get the number of seconds to delay
					log("Delay " + delayResponse.getDelay() + " seconds to " + pst.format(fdtf));
//		delayResponse.setDelay(0);     //  Override whatever the server returns
					sleep(delayResponse.getDelay(), false);

					//  Get the employer to retrieve from DLI
					Employer employer = null;
					EmployerDetailsDto employerDetailsDto = null;

					try {
						employer = reqUtils.getEmployer();
					} catch (IOException e1) {
						log(stackTrace(e1, false));
						employer = EMP_ERROR;
					} catch ( Exception e2 ) {
						log(stackTrace(e2, false));
					}

					log("\t\t - " + fixedWidth(employer.getFn(),7) + "\t" + 
								    fixedWidth(employer.getCounty(),14) + "\t" + 
								    fixedWidth(trim(employer.getName()),50) + "\t" + 
								    fixedWidth("[" + trim(employer.getCarrier()) + "]",42) + "\t" + 
								    fixedWidth(employer.getCode(),4) + "\t" + (employer.getUpdated() != null ? dtsdf.format(employer.getUpdated()) : "n/a"));

					if ( ! "ERROR".equals(employer.getName())) {
						String viewState = reqUtils.getSearchPage();
//						saveToFile("pcrb-viewstate-" + employer.getFn() + ".html", viewState);

						// Search on the employer name
						detailsPageCount = 0;
//						String html = "";
						// do {
						String summaryPage = reqUtils.getSummaryPage(employer.getName());

						sleep(0 + r.nextInt(2), false);

						String detailsPage = reqUtils.getDetailsPage(employer.getFn().toString());

//							DETAIL_LOOP:
						if (detailsPage == null) {
							saveToFile("pcrb-summary-" + employer.getFn() + ".html", summaryPage);

							log("\tSearch FN: " + employer.getFn().toString() + "   Results from " + firstFN + " to "
									+ lastFN);
							String[] permutations = createNamePermutations(employer.getName());
							for (String p : permutations) {
//									saveToFile("pcrb-research-" + employer.getFn() + "-" + p + ".html", summaryPage);
								summaryPage = reqUtils.getSummaryPage(p);
								detailsPage = reqUtils.getDetailsPage(employer.getFn().toString());
								if (detailsPage != null) {
									break;
								} else {
									saveToFile("pcrb-summary-" + employer.getFn() + "-" + p + ".html", summaryPage);
								}
							}
						}

						if (detailsPage != null && !"".equals(detailsPage)) {
							saveToFile("pcrb-details-" + employer.getFn() + ".html", detailsPage);

							if ( !detailsPage.contains("No data to display") ) {
							employerDetailsDto = reqUtils.getEmployerDetailsDto();

//				                setEmployerDetailsToEmployer(employer, employerDetailsDto);
							if ( employerDetailsDto != null ) {
								setEmployerCarrier(employer, employerDetailsDto.getPolicyDetails());
							}
							employer.setUpdated(new Timestamp(System.currentTimeMillis())); // Again just to be sure

							log("\t\t + " + fixedWidth(employer.getFn(), 7) + "\t"
									+ fixedWidth(employer.getCounty(), 14) + "\t"
									+ fixedWidth(trim(employer.getName()), 50) + "\t"
									+ fixedWidth("[" + trim(employer.getCarrier()) + "]", 42) + "\t"
									+ fixedWidth(employer.getCode(), 4) + "\t"
									+ (employer.getUpdated() != null ? dtsdf.format(employer.getUpdated()) : "n/a"));
							} else {
								employer.setCarrier("NO DATA TO DISPLAY");
							}
						} else {
							employer.setCarrier("PRIMARY PAGE NOT FOUND");
						}

						employer.setUpdated(Timestamp.valueOf(LocalDateTime.now()));
						delayResponse = postEmployer(employer);
						if (delayResponse.getDelay() > 50000) {
							log("DELAY ERROR!  " + delayResponse.getDelay() + " unacceptable.");
							delayJson = reqUtils.getRequestWithoutProxy(Folders.serverURL + "?option=delay");
							delayResponse = mapper.readValue(delayJson, DelayResponse.class);
						}

						if (detailsPage != null && !"".equals(detailsPage) && employerDetailsDto != null ) {
							postPolicyDetails(employerDetailsDto);
							logSql(employer, employerDetailsDto);
						}
						timeouts = 0;
						errors = 0;

					} else {
						delayJson = reqUtils.getRequestWithoutProxy(Folders.serverURL + "?option=delay");
						delayResponse = mapper.readValue(delayJson, DelayResponse.class);
					}

				} catch (TimeoutException te) {
					//  Reset Selenium
			    	reqUtils.resetProxySession();
					timeouts++;
					sleep((timeouts*60) + r.nextInt(60), true);
					
				} catch (Exception e) {
			    	reqUtils.resetProxySession();
					log(stackTrace(e, false));
					System.gc();

					do {
						if (++errors%10 == 0) {
							reqUtils.resetPhantomJS();
			                emailReport(stackTrace(e, false), "Luminati client exception limit reached (" + proxyPort + ")");
						}

						sleep((errors*30) + r.nextInt(errors*30), true);

						try {
							delayJson = reqUtils.getRequestWithoutProxy(Folders.serverURL + "?option=delay");
							delayResponse = mapper.readValue(delayJson, DelayResponse.class);
							sleep(5 + r.nextInt(30), true);
						} catch (Exception e1) {
							log(stackTrace(e1, false));
						}
					} while (delayResponse == null);
				}
			}
		} catch ( Exception e ) {			
			log(stackTrace(e, false));
		}
	}


	private static String fixedWidth(String s, int w) {
		try {
		String result = "";
		if (s == null) {
			while (result.length() < w ) {
				result += " ";
			} 
		} else if ( s.length() > w ) {
			result = s.substring(0, w-3) + "...";
		} else if ( s.length() == w ) {
			result = s;
		} else {
			result = s;
			while ( result.length() < w ) {
				result += " ";
			}
		}
		return result;
		} catch ( Exception e ) {
			return s;
		}
	}
	
//	private void setEmployerDetailsToEmployer(Employer employer, EmployerDetailsDto employerDetailsDto) {
//        employer.setFn(employerDetailsDto.getFileNumber());
////      employer.setFein(employerDetailsDto.getFein());
//        employer.setAddr(employerDetailsDto.getAddress());
//        employer.setPname(employerDetailsDto.getPrimaryName());
//        employer.setCity(employerDetailsDto.getCity());
//        employer.setState(employerDetailsDto.getState());
//        employer.setFn(employerDetailsDto.getFileNumber());
//    }


    public String setEmployerCarrier(Employer employer, List<PolicyDetails> policyHistory) {
		LocalDate ld = LocalDate.now();

		String carrier = "CANCELLED";
        if (policyHistory != null) {
        	for ( PolicyDetails policy : policyHistory ) {
	            try {
	            	String expDate = policy.getExpirationDate();
	            	String canDate = policy.getCancelDate();
	            	if (expDate != null && !"00/00/00".contentEquals(expDate) &&
	            			(canDate == null || canDate.trim().length() == 0 ) ) { 
	            		try {
	            			LocalDate cd = LocalDate.parse(expDate, pdtf);
	            			if (cd.isAfter(ld)) {
	            				carrier = policy.getInsuranceCarrier();
	            				employer.setCarrier(policy.getInsuranceCarrier());
	            				ld = cd;
	            			}
	            		} catch (Exception e) {
	            			e.printStackTrace();
	            		}
	            	}
	            } catch (Exception e) {
	                e.printStackTrace();
	                emailReport(stackTrace(e, false), "Luminati policy history exception");
	                throw e;
	            }
        	}
        	if ( "CANCELLED".contentEquals(carrier) ) {
                employer.setCarrier("CANCELLED");
                log("*** WARNING ***   Could not find active policy for " + employer.getName() + " (" + employer.getFn() + "):" + policyHistory.toString());
            }
        }
        return carrier;
    }

	private String[] createNamePermutations(String name) {
		ArrayList<String> cnp = new ArrayList<String>();

		String nn = name.
			replaceAll(" INC$","").
			replaceAll(" CO$", "").
			replaceAll(" LLP$","").
			replaceAll(" LLC$","").
			replaceAll(" LP$","").
			replaceAll(" CORP$","").
			replaceAll(" LTD$","").
			replaceAll(" PC$","").
			replaceAll(" D/B/A","").
	
			replaceAll(" INC[ \\.]","").
			replaceAll(" CO[ \\.]", "").
			replaceAll(" LLP[ \\.]","").
			replaceAll(" LLC[ \\.]","").
			replaceAll(" LP[ \\.]","").
			replaceAll(" CORP[ \\.]","").
			replaceAll(" LTD[ \\.]","").
			replaceAll(" PC[ \\.]","").
			replaceAll(" DBA ","").
			replaceAll("L/C/F","").
			replaceAll("D/B/A","");

		if ( nn.matches("^([A-Z|a-z]) & ([A-Z|a-z]) (.*)$")) {
			try {
				//  H & H SETTLEMENT SERVICES INC
				StringBuffer s1 = new StringBuffer();
				
				String[] wordTerms = nn.split("\\s");
				s1.append(wordTerms[0] + wordTerms[1] + wordTerms[2]);
				for ( int i = 3; i < wordTerms.length; i++ ) {
					s1.append(" " + wordTerms[i]);
				}
				cnp.add(s1.toString());
			} catch (Exception e) {
				e.printStackTrace();
			}

		} else if ( nn.matches("^([A-Z|a-z])\\s([A-Z|a-z]) (.*)$") ) {
			try {
				// V W INTERNATIONAL
				StringBuffer s1 = new StringBuffer();

				String[] wordTerms = nn.split("\\s");
				s1.append(wordTerms[0] + wordTerms[1]);
				for ( int i = 2; i < wordTerms.length; i++ ) {
					s1.append(" " + wordTerms[i]);
				}
				cnp.add(s1.toString());
			} catch (Exception e) {
				e.printStackTrace();
			}

		} else if ( nn.matches("^([^\\s]*)\\s([^\\s])\\s&\\s([^\\s]*)\\s([^\\s])\\s(.*)$") ) {
			try {
				// CHARLES N & ANN D MARSHALL
				String[] terms = nn.split(" ");
				cnp.add(terms[5] + " " + terms[0] + " " + terms[1] + " & " + terms[3] + " " + terms[4]);
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		} else if ( nn.matches("^([^\\s]*)\\s([^\\s]*)\\s([^\\s])\\s(.*)$") ) {
			try {
				//  BUCK PEARL S FOUNDATION INC
				String[] terms = nn.split(" ");
				cnp.add(terms[1] + " " + terms[2] + " " + terms[0] + nn.substring(terms[0].length() + terms[1].length() + terms[2].length() + 2));
			} catch (Exception e) {
				e.printStackTrace();
			}

		} else if ( nn.matches("^([^\\s]*)\\s([^\\s]*)\\s([^\\s])$") ) {
			try {
				//  AMMONS JOHN L INC
				String[] terms = nn.split(" ");
				cnp.add(terms[1] + " " + terms[2] + " " + terms[0] + nn.substring(terms[0].length() + terms[1].length() + terms[2].length() + 2));
			} catch (Exception e) {
				e.printStackTrace();
			}

		} else if ( nn.matches("^([A-Z|a-z]*),([A-Z|a-z]*)$") ) {

			try {
				String[] terms = nn.split(",");
				cnp.add(terms[1] + " " + terms[0]);
			} catch (Exception e) {
				e.printStackTrace();
			}

		} else if ( nn.matches("^([A-Z|a-z]*),([A-Z|a-z]*),([A-Z|a-z]*)$") ) {
			try {
				// ROCK,ROBERT,C
				String[] terms = nn.split(",");
				cnp.add(terms[1] + " " + terms[2] + " " + terms[0]);
			} catch (Exception e) {
				e.printStackTrace();
			}

		} else {
			try {
				String[] terms = nn.split(" ");
				if ( terms.length >= 2 ) {
					cnp.add(terms[0] + " " + terms[1]);
					cnp.add(terms[1] + " " + terms[0]);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return cnp.toArray(new String[0]);
	}


	public String extractAndSetCarrier(Employer employer, HashMap<String,Object> pcrbInfo) {
		String carrier = "";
		if ( pcrbInfo != null ) {
			try {			 
				@SuppressWarnings("unchecked")
				HashMap<String,String>[] details = (HashMap<String,String>[])pcrbInfo.get("details");

				int index = -1;
				LocalDate ld = LocalDate.now();

				for ( int i = 0; i < details.length; i++ ) {
					String ped = details[i].get("Policy Expiration Date");
					String pcd = details[i].get("Policy Cancellation Date");
					if ( ! "00/00/00".equals(ped) && pcd == null) {
						try {
							LocalDate cd = LocalDate.parse(ped, pdtf);
							if ( cd.isAfter(ld) ) {
								index = i;
								ld = cd;
							}
						} catch (Exception e) {
							log(stackTrace(e, false));
						}
					}
				}
				if ( index >= 0 ) {
					carrier = trim(details[index].get("Insurance Carrier"));
					employer.setCarrier(carrier);
				} else { // if ( maxIndex >= 0 ) {
					employer.setCarrier("CANCELLED");
				}
			} catch ( Exception e ) {
				log(stackTrace(e, false));
				emailReport(stackTrace(e, false), "Luminati exception");
				throw e;
			}
		}
		return carrier;
	}
	
	
	public static String trim(String s) {
		return ( s == null ? null : s.trim());
	}
	
	
	public DelayResponse postEmployer(Employer employer) {
		DelayResponse delay = new DelayResponse();
		try {
			try {
				String postData = "data=" + URLEncoder.encode(mapper.writeValueAsString(employer), "UTF-8");
				byte[] data = reqUtils.postURLwithoutProxy(Folders.serverURL, postData);
				String result = new String(data); 
				if ( !result.contains("<")) {
					delay = mapper.readValue(result, DelayResponse.class);
				}

			} catch ( Exception e ) {
				log(stackTrace(e, false));
				delay = new DelayResponse();
				delay.setDelay(94);
			}
		} catch ( Exception pe) {
			delay = new DelayResponse();
			delay.setDelay(94);
		}
		return delay;
	}
	

	public void postPolicyDetails(EmployerDetailsDto details) {
		try {
			String postData = "option=policies&details=" + URLEncoder.encode(mapper.writeValueAsString(details), "UTF-8");
			byte[] data = reqUtils.postURLwithoutProxy(Folders.serverURL, postData);
			String result = new String(data);
			System.out.println(result);

		} catch (Exception e) {
			log(stackTrace(e, false));
		}
	}
	

	public DelayResponse getLocalDelay(int min, int max) {
		DelayResponse dr = new DelayResponse();
		dr.setMyIp("127.0.0.1");
		dr.setDelay(min + r.nextInt(max-min));
		return dr;
	}

	
	public void saveToFile(String filename, String content) {
		
		LocalDate ld = LocalDate.now();
		String folderName = Folders.COLLECTION_DIRECTORY + ld.format(dtf);
		String finalFile = folderName + "/" + filename;
		try {
			Files.createDirectory(Paths.get(folderName));
		} catch ( FileAlreadyExistsException ioe) {
			
		} catch ( IOException ioe) {
			finalFile = filename;
		}

		PrintWriter pw = null;
		try {
			pw = new PrintWriter(finalFile);
			pw.println(content);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (pw != null) {
				pw.close();
			}
		}
	}

	public void sleep(int secs, boolean sendToLog) {
		try {
			if (sendToLog) {
				log("Sleeping " + secs + " seconds...");
			}
			Thread.sleep(secs*1000);
		} catch (Exception e) {
		}
	}

	public static String stackTrace(Exception e, boolean synesisOnly) {
		StackTraceElement[] trace = e.getStackTrace();
		StringBuffer sb = new StringBuffer("\n");
		for ( StackTraceElement s : trace ) {
//			if (!synesisOnly || s.toString().contains("synesis")) {
				sb.append(s.toString() + "\n");
//			}
		}
		return sb.toString();
	}
	
    public static void emailReport(String text, String subject) {  
        try {
			String to = "klj1962@gmail.com";//change accordingly  
			String from = "kjohnson@synesis-inc.com";

			String username = "klj1962@gmail.com";
			String password = "biyhuoxjcdtqpcfw";
			
			//Get the session object  
			Properties properties = System.getProperties();  
			properties.put("mail.smtp.auth", "true");
			properties.put("mail.smtp.starttls.enable", "true");
			properties.put("mail.smtp.starttls.required", "true");
			properties.put("mail.smtp.host", "smtp.gmail.com");
			properties.put("mail.smtp.ssl.protocols", "TLSv1.2");
			properties.put("mail.smtp.port", "587");

			Session session = Session.getInstance(properties,
				  new javax.mail.Authenticator() {
					protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
						return new javax.mail.PasswordAuthentication(username, password);
					}
				  });

      //compose the message  
			try
			{  
			   MimeMessage message = new MimeMessage(session);  
			   message.setFrom(new InternetAddress(from));  
			   message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(to));  
			   message.setSubject(subject);  
			   message.setText(text);  
   
			   // Send message  
			   Transport.send(message);
   
			} catch (MessagingException mex) {
				System.out.println(stackTrace(mex, false));
				mex.printStackTrace();
			}
		} catch (Exception e) {
			System.out.println(stackTrace(e,false));
			e.printStackTrace();
		}
   }
	

	public static void emailFile(String toAddress, String subject, String body,String fileName) throws Exception {

		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");

		Session session = Session.getDefaultInstance(props,
				new Authenticator() {
					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(
								"klj1962@gmail.com",
								"biyhuoxjcdtqpcfw");
					}
				});

		Message message = new MimeMessage(session);
		message.setFrom(new InternetAddress("support@kswcrc.com"));

		message.setSubject(subject);
		message.setText(body);
		message.setRecipient(RecipientType.TO, new InternetAddress(toAddress));
		
		 // Create the message part
        BodyPart messageBodyPart = new MimeBodyPart();

        // Now set the actual message
        messageBodyPart.setText(body);

		
		  // Create a multipar message
        Multipart multipart = new MimeMultipart();

        // Set text message part
        multipart.addBodyPart(messageBodyPart);
		
		 messageBodyPart = new MimeBodyPart();
		 DataSource source = new FileDataSource(fileName);
         messageBodyPart.setDataHandler(new DataHandler(source));
         messageBodyPart.setFileName(fileName);
         multipart.addBodyPart(messageBodyPart);

         // Send the complete message parts
         message.setContent(multipart);
		
		Transport.send(message);
	}


	private void logSql(Employer emp, EmployerDetailsDto dto) {
		PrintWriter pw = null;
		try {
			String logFileName = Folders.BASE_DIRECTORY + "sql-" + LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE) + "-" + proxyPort + ".sql";
			pw = new PrintWriter(new FileOutputStream(new File(logFileName), true));

			String updateStmt = "UPDATE employers SET carrier=" + "'" + emp.getCarrier().trim() + "'"
					+ ", updated=" + "'" + (emp.getUpdated() != null ? dtsdf.format(emp.getUpdated()) : "n/a") + "'"
					+ " WHERE fn=" + "'" + emp.getFn() + "';";
			pw.println(updateStmt);
			pw.flush();
			pw.close();


			String sqlFileName = Folders.BASE_DIRECTORY + "sql-policies-" + LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE) + "-" + proxyPort + ".sql";
			pw = new PrintWriter(new FileOutputStream(new File(sqlFileName), true));

			String deletePolicies = "DELETE FROM policies WHERE fn=" + "'" + emp.getFn() + "';";
			pw.println(deletePolicies);

			for (PolicyDetails pd : dto.getPolicyDetails()) {
				try {
					String effDateStr = null;
					String expDateStr = null;
					String canDateStr = null;
					
					try {
						LocalDate fd = LocalDate.parse(pd.getEffectiveDate(), pdtf);
						effDateStr = fd.format(dtf);
					} catch (Exception e) {
						effDateStr = null;
					}
					try {
						LocalDate xd = LocalDate.parse(pd.getExpirationDate(), pdtf);
						expDateStr = xd.format(dtf);
					} catch (Exception e) {
						expDateStr = null;
					}
					try {
						LocalDate cd = LocalDate.parse(pd.getCancelDate(), pdtf);
						canDateStr = cd.format(dtf);
					} catch (Exception e) {
						canDateStr = null;
					}
					
					String policyStmt = "INSERT INTO policies (fn,line,polnum,carrier,naic,effDate,expDate,canDate) values ("
							+ emp.getFn() + "," + pd.getLineNumber() + ","
							+ "'" + pd.getPolicyNumber() + "',"
							+ "'" + pd.getInsuranceCarrier() + "'," 
							+ "'" + pd.getNaic() + "'," 
							+ (effDateStr != null ? "'" + effDateStr + "'," : "null") 
							+ (expDateStr != null ? "'" + expDateStr + "'," : "null") 
							+ (canDateStr != null ? "'" + canDateStr + "'" : "null")
							+ ");";
							
					pw.println(policyStmt);
				} catch ( Exception e ) {
					pw.println("Policy Line exception");
				}
			}
			pw.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (pw != null) {
				pw.close();
			}
		}
	}

	
	private void log(String s) {
		PrintWriter pw = null;
		try {
			System.out.println(LocalDateTime.now().format(fdtf) + " (" + this.proxyPort + ")\t" + s);
			String logFileName = Folders.BASE_DIRECTORY + "lumThr-" + proxyPort + ".log";
			pw = new PrintWriter(new FileOutputStream(new File(logFileName), true));
			pw.println(LocalDateTime.now().format(fdtf) + " (" + this.proxyPort + ")\t" + s);
			pw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (pw != null) {
				pw.close();
			}
		}
	}
	
}
