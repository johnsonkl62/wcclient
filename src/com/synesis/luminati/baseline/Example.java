package com.synesis.luminati.baseline;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;

class Client {
    public static final String username = "lum-customer-synesis-zone-residential";
    public static final String password = "14b54989c7df";
    public static final int port = 22225;
    public static final String user_agent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36";
    public static final int max_failures = 3;
    public static final int req_timeout = 60*1000;
    public String session_id;
    public HttpHost super_proxy;
    public CloseableHttpClient client;
    public String country;
    public int fail_count;
    public int n_req_for_exit_node;
    public Random rng;
    public String host;

    public Client(String country, String host) {
        this.country = country;
        this.host = host;
        rng = new Random();
        switch_session_id();
    }

    public void switch_session_id() {
        session_id = Integer.toString(rng.nextInt(Integer.MAX_VALUE));
        n_req_for_exit_node = 0;
        super_proxy = new HttpHost(host, port);
        update_client();
    }

    public void update_client() {
        close();
        String login = username+(country!=null ? "-country-"+country : "")
                +"-session-" + session_id;
        CredentialsProvider cred_provider = new BasicCredentialsProvider();
        cred_provider.setCredentials(new AuthScope(super_proxy),
                new UsernamePasswordCredentials(login, password));
        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(req_timeout)
                .setConnectionRequestTimeout(req_timeout)
                .build();
        PoolingHttpClientConnectionManager conn_mgr =
                new PoolingHttpClientConnectionManager();
        conn_mgr.setDefaultMaxPerRoute(Integer.MAX_VALUE);
        conn_mgr.setMaxTotal(Integer.MAX_VALUE);
        client = HttpClients.custom()
                .setConnectionManager(conn_mgr)

                .setProxy(super_proxy)
                .setDefaultCredentialsProvider(cred_provider)
                .setDefaultRequestConfig(config)
                .build();
    }

    public CloseableHttpResponse request(String url) throws IOException {
        try {
            HttpGet request = new HttpGet(url);
            request.setHeader("User-Agent", user_agent);
            CloseableHttpResponse response = client.execute(request);
            handle_response(response);
            return response;
        } catch (IOException e) {
            handle_response(null);
            throw e;
        }
    }

    public void handle_response(HttpResponse response) {
        if (response != null && !status_code_requires_exit_node_switch(
                response.getStatusLine().getStatusCode())) {
            // success or other client/website error like 404...
            n_req_for_exit_node++;
            fail_count = 0;
            return;
        }
        switch_session_id();
        fail_count++;
    }

    public boolean status_code_requires_exit_node_switch(int code) {
        return code == 403 || code == 429 || code==502 || code == 503;
    }

    public boolean have_good_super_proxy() {
        return super_proxy != null && fail_count < max_failures;
    }

    public void close() {
        if (client != null)
            try { client.close(); } catch (IOException e) {}
        client = null;
    }
}

public class Example implements Runnable {
    public static final int n_parallel_exit_nodes = 3;
    public static final int n_total_req = 20;
    public static final int switch_ip_every_n_req = 3;
    public static AtomicInteger at_req = new AtomicInteger(0);
    public static String host;

    public static void main(String[] args) {
        System.out.println("To enable your free eval account and get CUSTOMER, YOURZONE and YOURPASS, please contact sales@luminati.io");
        try {
            int proxy_session_id = new Random().nextInt(Integer.MAX_VALUE);
            InetAddress address = InetAddress.getByName("customer-synesis-session-"+proxy_session_id+"-servercountry-us.zproxy.luminati.io");
            host = address.getHostAddress();
            ExecutorService executor =
                Executors.newFixedThreadPool(n_parallel_exit_nodes);
            for (int i = 0; i < n_parallel_exit_nodes; i++)
                executor.execute(new Example());
            executor.shutdown();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        Client client = new Client("us", host);
        while (at_req.getAndAdd(1) < n_total_req) {
            if (!client.have_good_super_proxy())
                client.switch_session_id();
            if (client.n_req_for_exit_node == switch_ip_every_n_req)
                client.switch_session_id();
            CloseableHttpResponse response = null;
            try {
//            	response = client.request("https://api.ipify.org?format=json");
//            	response = client.request("http://lumtest.com/myip.json");
            	response = client.request("http://23.23.185.50/myip.json");
                int code = response.getStatusLine().getStatusCode();
                if ( code == 200 ) {
                	String r = EntityUtils.toString(response.getEntity());
//                	int sIndex = r.indexOf("<p class=\"ipaddress\">") + "<p class=\"ipaddress\">".length();
//                	int eIndex = r.indexOf("</p>",sIndex);
                	
//                	Header[] headers = response.getHeaders("x-hola-ip");
//                	for ( Header header : headers) {
//                		System.out.println(header.getName() + " : " + header.getValue());
//                	}
                	
//                    System.out.println(r.substring(sIndex,eIndex));
                    System.out.println(r);
                } else {
                	System.out.println(code);
                }
            } catch (IOException e) {
                System.out.println(e.getMessage());
            } finally {
                try {
                    if (response != null)
                        response.close();
                } catch (Exception e) {}
            }
        }
        client.close();
    }
}
