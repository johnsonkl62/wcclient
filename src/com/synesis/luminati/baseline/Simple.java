package com.synesis.luminati.baseline;


public class Simple {

//    private static final String serverURL = "http://207.244.242.208/WCC/Luminati";
//
//	public static DateTimeFormatter pdtf = DateTimeFormatter.ofPattern("MM/dd/yy");
//	public static DateTimeFormatter fdtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
//	
//	private static ObjectMapper mapper = new ObjectMapper();
//
//	public static Integer detailsPageCount = 0;
//	public static final Integer detailsPageLimit = 100;
//
//	public static String firstFN = null;
//	public static String lastFN = null;
//
//	static public PrintWriter updateFile = null;
//	static private String updateFileName = "C:\\Synesis\\updates-" + LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE) + ".sql";
//	static private String lastFileName = "C:\\Synesis\\updates-" + LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE) + ".sql";
//
//	static private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//	static private DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//
//	static private Random r = new Random(Calendar.getInstance().getTimeInMillis());
//	
////	public static void main(String[] args) throws IOException {
////
////		try {
////			StringBuffer sb = new StringBuffer();
////
////			//  Open file
////			BufferedReader br = new BufferedReader(new FileReader("pcrb-summary-3194854.html"));
////			String line;
////			while ( (line = br.readLine()) != null ) {
////				sb.append(line);
////			}
////			br.close();
////
////			String detailsURL = extractDetailsLink(sb.toString(), "invalid");
////			System.out.println("First FN: " + firstFN);
////			System.out.println("Last FN:  " + lastFN);
////
////		} catch (Exception e) {
////
////			e.printStackTrace();
////		}
////		
////	}
//	
//	
//	
//	public static void main(String[] args) throws IOException {
//		
//		if ( args.length < 1 ) {
//			System.exit(1);
//		}
//
//		RequestUtils.setProxyPort(Integer.parseInt(args[0]));
//		
//		
//		DLI dli = new DLI();
//		try {
//
//			//  Get delay from WCC
//			String delayJson = RequestUtils.getRequest(serverURL + "?option=delay");
//
//			DelayResponse delayResponse = mapper.readValue(delayJson, DelayResponse.class);
//
//			// Scraping sequence ...
//			while (delayResponse != null) {
//				try {
//					LocalDateTime now = LocalDateTime.now();
//					LocalDateTime pst = LocalDateTime.now();
//					pst = pst.plusSeconds(delayResponse.getDelay());
//
//					//  Get the number of seconds to delay
//					System.out.println(now.format(fdtf) + " - IP " + delayResponse.getMyIp() + "    Delay " + delayResponse.getDelay() + " seconds until " + pst.format(fdtf));
////	/*Uncomment to test*/		delayResponse.setDelay(0);     //  Override whatever the server returns
//					sleep(delayResponse.getDelay());
//
//					//  Get the employer to retrieve from DLI
//					String employerString = RequestUtils.getRequestWithoutProxy(serverURL + "?option=employer");
//					Employer employer = mapper.readValue(employerString, Employer.class);
//					now = LocalDateTime.now();
//					System.out.print(now.format(fdtf) + " - " + employer.getFn() + "\t" + employer.getCounty() + "\t" + employer.getName());
//
//					if ( ! "ERROR".equals(employer.getName())) {
//					//  Load initial DLI search page
//					dli.loadViewState();
//					sleep(1+r.nextInt(3));
//
//					String detailsURL = null;
//
//					//  Search on the employer name
//					try {
//						detailsPageCount = 0;
////						do {
//							String summaryPage = dli.getSummaryPage(employer.getName(),
//									employer.getFn().toString());
//							saveToFile("pcrb-summary-" + employer.getFn() + ".html", summaryPage);
//
//							sleep(2+r.nextInt(3));
//							//KLJ   This can be improved by tracking the first and last FNs on each search page to 
//							//   determine if the search FN has been reached.
//							detailsURL = extractDetailsLink(summaryPage, employer.getFn().toString());
//							if ( detailsURL == null ) {
//								System.out.println("\r\nSearch FN: " + employer.getFn().toString() +  "   Results from " + firstFN + " to " + lastFN);
//							}
////						} while (detailsURL == null &&  (++detailsPageCount < detailsPageLimit ));
//					} catch (Exception e) {
//						detailsURL = null;
//					}
//
//					//  Search the summary page for the exact match based on FN
//					String resultsPage = dli.getDetailPage(detailsURL);
//					saveToFile("pcrb-details-" + employer.getFn() + ".html", resultsPage);
//					HashMap<String, Object> results = extractDetails(detailsURL, resultsPage);
//
//					//  Post result / get new delay
//					extractAndSetCarrier(employer, results);
//		    		addToUpdateFile(formatUpdateStatement(employer));
//					delayResponse = postEmployer(employer);
//					} else {
//						emailReport("ERROR OCCURRED", "Received ERROR employer ... ");
//						delayJson = RequestUtils.getRequest(serverURL + "?option=delay");
//						delayResponse = mapper.readValue(delayJson, DelayResponse.class);
//					}
////				delayJson = dli.getRequest(serverURL + "?option=delay");
////				delayResponse = mapper.readValue(delayJson, DelayResponse.class);
//				} catch (java.net.SocketException se) {
//					emailReport(stackTrace(se), "SocketException OCCURRED ... ");
//					dli = null;
//					System.gc();
//					dli = new DLI();
//					delayJson = RequestUtils.getRequest(serverURL + "?option=delay");
//					delayResponse = mapper.readValue(delayJson, DelayResponse.class);
//				} catch (Exception e) {
//					e.printStackTrace();
//					delayJson = RequestUtils.getRequest(serverURL + "?option=delay");
//					delayResponse = mapper.readValue(delayJson, DelayResponse.class);
//				}
//			}
//		} catch ( Exception e ) {			
//			System.out.println(e.toString());
//		}
//	}
//
//
//	public static void extractAndSetCarrier(Employer employer, HashMap<String,Object> pcrbInfo) {
//		if ( pcrbInfo != null ) {
//			employer.setInfo(pcrbInfo);
//
//			try {			 
//				@SuppressWarnings("unchecked")
//				HashMap<String,String>[] details = (HashMap<String,String>[])pcrbInfo.get("details");
//
//				int index = -1;
//				LocalDate ld = LocalDate.now();
//
//				for ( int i = 0; i < details.length; i++ ) {
//					String ped = details[i].get("Policy Expiration Date");
//					String pcd = details[i].get("Policy Cancellation Date");
//					if ( ! "00/00/00".equals(ped) && pcd == null) {
//						try {
//							LocalDate cd = LocalDate.parse(ped, pdtf);
//							if ( cd.isAfter(ld) ) {
//								index = i;
//							}
//						} catch (Exception e) {
//							//  Skip entry
//							System.out.println(e.toString());
//						}
//					}
//				}
//				if ( index >= 0 ) {
//					employer.setCarrier(details[index].get("Insurance Carrier").trim());
//				}
//				System.out.println("\t" + employer.getCarrier());
//			} catch ( Exception e ) {
//				System.out.println(e.toString());
//				emailReport(stackTrace(e), "Luminati exception");
//				throw e;
//			}
//		}
//	}
//	
//	
//	public static DelayResponse postEmployer(Employer employer) {
//		DelayResponse delay = new DelayResponse();
//		try {
//			try {
//				String postData = "data=" + URLEncoder.encode(mapper.writeValueAsString(employer), "UTF-8");
//				byte[] data = RequestUtils.postURLwithoutProxy(serverURL, postData);
//				String result = new String(data); 
//				if ( !result.contains("<")) {
//					delay = mapper.readValue(result, DelayResponse.class);
//				}
//
//			} catch ( Exception e ) {
//				System.out.println(stackTrace(e));
//				delay = new DelayResponse();
//				delay.setDelay(94);
//			}
//		} catch ( Exception pe) {
//			delay = new DelayResponse();
//			delay.setDelay(94);
//		}
//		return delay;
//	}
//	
//
//	public static String extractDetailsLink(String summaryPage, String fn) {
//		//   Each result page contains at most 50 results which are sequential based on FN, even if the search criteria
//		//   returns thousands.  The next search on the identical criteria will retrieve the NEXT 50 in the sequence in
//		//   a round robin method.
//		//
//		//   If the FN is not found, the first and last lines will show whether the FN is just missing completely, i.e.
//		//              first line FN  <   FN   <  last line FN
//		//   or if the search must continue, i.e.
//		//              FN  < first line FN    ||   FN  > last line FN
//		//
//		String detailsURL = null;
////		Integer minPageFN = 999999999;
////		Integer maxPageFN = -1;
//		if ( summaryPage != null ) {
//			try {
//				Document doc = DOMUtils.getDocument(new ByteArrayInputStream(summaryPage.getBytes()));
//	
//				detailsURL = (String) DOMUtils.xpathSearch(doc, String.format("//b[contains(text(),'%s')]/../../..//a/@onclick", fn), XPathConstants.STRING);
//	
//				try {
//					Pattern r = Pattern.compile("^location\\.href=\\'(.*)\\'$");
//					Matcher m = r.matcher(detailsURL);
//					m.find();
//					detailsURL = m.group(1);
//				} catch (Exception e) {
//					detailsURL = null;	// no output necessary
//
//					firstFN = (String) DOMUtils.xpathSearch(doc, "//b/a/u/../../../../../td[position()=5]/font/b", XPathConstants.STRING);
//					lastFN  = (String) DOMUtils.xpathSearch(doc, "(//b/a/u)[last()]/../../../../../td[last()]/font/b", XPathConstants.STRING);
//
//				}
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		}	
//	    return detailsURL;
//	}
//	
//
//	public static HashMap<String, Object> extractDetails(String url, String data) {
//        if (data == null ) 
//        	return null;
//
//        HashMap<String, Object> output = new HashMap<String, Object>();
//        output.put("url", url);
//
//        Document doc = DOMUtils.getDocument(new ByteArrayInputStream(data.getBytes()));
//
//        HashMap<String, String> info = new HashMap<String, String>();
//        DTMNodeList fieldNameEl = (DTMNodeList) DOMUtils.xpathSearch(doc, "//table[@class='record_list']//th[@class='list_header']", XPathConstants.NODESET);
//
//        for (int i = 0; i < fieldNameEl.getLength(); i++) {
//            String fieldName = fieldNameEl.item(i).getFirstChild().getNodeValue();
//            String fieldValue = fieldNameEl.item(i).getNextSibling().getFirstChild().getFirstChild().getNodeValue();
//            info.put(fieldName.substring(0, fieldName.length() - 1), fieldValue);
//        }
//
//        output.put("info", info);
//
//        DTMNodeList columnNameEl = (DTMNodeList) DOMUtils.xpathSearch(doc, "//table[@id='GridPolicySearch']//tr[@class='records_header']/th", XPathConstants.NODESET);
//        String[] columns = new String[columnNameEl.getLength()];
//        for (int i = 0; i < columnNameEl.getLength(); i++) {
//            columns[i] = columnNameEl.item(i).getFirstChild().getFirstChild().getNodeValue();
//        }
//
//        DTMNodeList rowEl = (DTMNodeList) DOMUtils.xpathSearch(doc, "//table[@id='GridPolicySearch']//tr[not(@class)]", XPathConstants.NODESET);
//        HashMap<String, String>[] details = new HashMap[rowEl.getLength()];
//        for (int i = 0; i < rowEl.getLength(); i++) {
//            HashMap<String, String> item = new HashMap<String, String>();
//            DTMNodeList cellEl = (DTMNodeList) DOMUtils.xpathSearch(rowEl.item(i), "./td//b", XPathConstants.NODESET);
//            for (int n = 0; n < cellEl.getLength(); n++) {
//                item.put(columns[n], cellEl.item(n).getFirstChild().getNodeValue());
//            }
//            details[i] = item;
//        }
//
//        output.put("details", details);
//
//        return output;
// 	}
//	
//	
//	public static void saveToFile(String filename, String content) {
//		
//			LocalDate ld = LocalDate.now();
//		String folderName = ld.format(dtf);
//		String finalFile = folderName + "\\" + filename;
//		try {
//			Files.createDirectory(Paths.get(folderName));
//		} catch ( FileAlreadyExistsException ioe) {
//			
//		} catch ( IOException ioe) {
//			finalFile = filename;
//		}
//
//		PrintWriter pw = null;
//		try {
//			pw = new PrintWriter(finalFile);
//			pw.println(content);
//		} catch (IOException e) {
//			e.printStackTrace();
//		} finally {
//			if (pw != null) {
//				pw.close();
//			}
//		}
//	}
//
//	public static void sleep(int secs) {
//		try {
//			Thread.sleep(secs*1000);
//		} catch (Exception e) {
//		}
//	}
//
//	public static String stackTrace(Exception e) {
//		StackTraceElement[] trace = e.getStackTrace();
//		StringBuffer sb = new StringBuffer();
//		for ( StackTraceElement s : trace ) {
//			if (s.toString().contains("synesis")) {
//				sb.append(s.toString() + "\n");
//			}
//		}
//		return sb.toString();
//	}
//	
//    public static void emailReport(String text, String subject) {  
//        String to = "klj1962@gmail.com";//change accordingly  
//        String from = "kjohnson@synesis-inc.com";
//
//        String username = "klj1962@gmail.com";
//        String password = "sisuuzcvnvnqiukd";   biyhuoxjcdtqpcfw
//        
//       //Get the session object  
//        Properties properties = System.getProperties();  
//        properties.put("mail.smtp.auth", "true");
//        properties.put("mail.smtp.starttls.enable", "true");
//        properties.put("mail.smtp.host", "smtp.gmail.com");
//        properties.put("mail.smtp.port", "587");
//
//        Session session = Session.getInstance(properties,
//    		  new javax.mail.Authenticator() {
//    			protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
//    				return new javax.mail.PasswordAuthentication(username, password);
//    			}
//    		  });
//
//       //compose the message  
//        try
//        {  
//           MimeMessage message = new MimeMessage(session);  
//           message.setFrom(new InternetAddress(from));  
//           message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(to));  
//           message.setSubject(subject);  
//           message.setText(text);  
//    
//           // Send message  
//           Transport.send(message);
//    
//        } catch (MessagingException mex) {
//			System.out.println(stackTrace(mex));
//        }
//   }
//	
//
//	public static void addToUpdateFile(String s) {
//		try {
//    		updateFileName = "C:\\Synesis\\updates-" + LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE) + ".sql";
//    		if ( !updateFileName.equalsIgnoreCase(lastFileName) ) {
//    			emailFile("klj1962@gmail.com", "KSWCRC SQL updates ...", "See attached", lastFileName);
//    		}
//			updateFile = new PrintWriter(new FileOutputStream(new File(updateFileName),true));
//			updateFile.println(s);
//			updateFile.flush();
//			updateFile.close();
//		} catch (Exception e) {
//		}
//	}
//
//	public static String formatUpdateStatement(Employer emp) {
//		String updateStatement = null;
//		
//		if (emp.getCarrier() != null) {
//			if (emp.getUpdated() == null) {
//				emp.setUpdated(new Timestamp(System.currentTimeMillis()));
//			}
//			String html = formatInfo(emp);
//			updateStatement = String.format(
//					"UPDATE employers SET carrier='%s', updated='%s', ip='127.0.0.1', info='%s' WHERE fn='%s'",
//					emp.getCarrier(), LocalDate.now().format(dtf), html, emp.getFn());
//		} else {
//			// update time only
//			String html = formatInfo(emp);
//				updateStatement = String.format(
//						"UPDATE employers SET updated='%s', WHERE fn='%s'",
//						LocalDate.now().format(dtf), html, emp.getFn());
//		}
//		return updateStatement;
//	}
//	
//	
//	
//	private static String formatInfo(Employer emp) {
//		HashMap<String, Object> pcrbInfo = emp.getInfo();
//
//		StringBuffer html = new StringBuffer();
//
//		if (pcrbInfo != null) {
//			try {
//				// System.out.println("URL: " + hm.get("url").toString());
//
//				@SuppressWarnings("unchecked")
//				HashMap<String, String> info = (HashMap<String, String>) pcrbInfo.get("info");
//
//				html.append("<h2>Policy Coverage (as of " + sdf.format(emp.getUpdated()) + ")</h2>");
//				html.append("<table>");
//				for (String is : info.keySet()) {
//					html.append("<tr><td>" + is + "</td><td>" + (info.get(is) != null ? info.get(is) : "") + "</td></tr>");
//				}
//				html.append("</table>");
//				html.append("<br/>");
//
//				@SuppressWarnings("unchecked")
//				HashMap<String, String>[] details = (HashMap<String, String>[]) pcrbInfo.get("details");
//
//				String PN = "Policy Number";
//				String IC = "Insurance Carrier";
//				String NAIC = "NAIC";
//				String PED = "Policy Effective Date";
//				String PAD = "Policy Anniversary Date";
//				String PXD = "Policy Expiration Date";
//				String PCD = "Policy Cancellation Date";
//
//				html.append("<table><tr><th>" + PN + "</th><th>" + IC + "</th><th>" + NAIC + "</th><th>" + PED
//						+ "</th><th>" + PAD + "</th><th>" + PXD + "</th><th>" + PCD + "</th></tr>");
//
//				for (HashMap<String, String> detail : details) {
//					html.append("<tr>");
//					html.append("<td>" + (detail.get(PN) != null ? detail.get(PN) : "") + "</td>");
//					html.append("<td>" + (detail.get(IC) != null ? detail.get(IC) : "") + "</td>");
//					html.append("<td>" + (detail.get(NAIC) != null ? detail.get(NAIC) : "") + "</td>");
//					html.append("<td>" + (detail.get(PED) != null ? detail.get(PED) : "") + "</td>");
//					html.append("<td>" + (detail.get(PAD) != null ? detail.get(PAD) : "") + "</td>");
//					html.append("<td>" + (detail.get(PXD) != null ? detail.get(PXD) : "") + "</td>");
//					html.append("<td>" + (detail.get(PCD) != null ? detail.get(PCD) : "") + "</td>");
//					html.append("</tr>");
//				}
//			} catch (Exception e) {
//				e.printStackTrace();
//				throw e;
//			}
//		}
//		return html.toString();
//	}
//
//	public static void emailFile(String toAddress, String subject, String body,String fileName) throws Exception {
//
//		Properties props = new Properties();
//		props.put("mail.smtp.host", HOST);
//		props.put("mail.smtp.socketFactory.port", "465");
//		props.put("mail.smtp.socketFactory.class",
//				"javax.net.ssl.SSLSocketFactory");
//		props.put("mail.smtp.auth", "true");
//		props.put("mail.smtp.port", "465");
//
//		Session session = Session.getDefaultInstance(props,
//				new Authenticator() {
//					@Override
//					protected PasswordAuthentication getPasswordAuthentication() {
//						return new PasswordAuthentication(
//								SMTP_USERNAME, SMTP_PASSWORD);
//					}
//				});
//
//		Message message = new MimeMessage(session);
//		message.setFrom(new InternetAddress("support@kswcrc.com"));
//
//		message.setSubject(subject);
//		message.setText(body);
//		message.setRecipient(RecipientType.TO, new InternetAddress(toAddress));
//		
//		 // Create the message part
//        BodyPart messageBodyPart = new MimeBodyPart();
//
//        // Now set the actual message
//        messageBodyPart.setText(body);
//
//		
//		  // Create a multipar message
//        Multipart multipart = new MimeMultipart();
//
//        // Set text message part
//        multipart.addBodyPart(messageBodyPart);
//		
//		 messageBodyPart = new MimeBodyPart();
//		 DataSource source = new FileDataSource(fileName);
//         messageBodyPart.setDataHandler(new DataHandler(source));
//         messageBodyPart.setFileName(fileName);
//         multipart.addBodyPart(messageBodyPart);
//
//         // Send the complete message parts
//         message.setContent(multipart);
//		
//		Transport.send(message);
//
//		System.out.println("Mail Sent");
//
//	
//		
//	}
//	

}
