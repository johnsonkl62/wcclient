package com.synesis.luminati;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

import com.synesis.config.Folders;

public class LuminatiMain {

	private static int threadCount = 1;
	
	private static Random r = new Random(Calendar.getInstance().getTimeInMillis());
	
	private static ArrayList<Thread> threads = new ArrayList<Thread>();
	
	public static void main(String[] args) {

		if ( System.getProperty("os.name").contains("Windows")) {
			String list_tasks = "tasklist /FI \"IMAGENAME eq phantomjs.exe\"";
			String kill_tasks = "taskkill /F /FI \"IMAGENAME eq phantomjs.exe\"";

			//
			//  Kill all prior Selenium instances
			//
			try {
			    String line;
			    Process p = Runtime.getRuntime().exec(list_tasks);
			    BufferedReader input =
			            new BufferedReader(new InputStreamReader(p.getInputStream()));
			    while ((line = input.readLine()) != null) {
			        System.out.println("Killing ... " + line);
			    }
			    input.close();

			    Runtime.getRuntime().exec(kill_tasks);
			    System.out.println("All phantomjs.exe processes terminated successfully.");
			} catch (Exception err) {
			    err.printStackTrace();
			}
		} else {
			
		}
		
		//
		//  Process arguments
		//     -s <server>  -n <threads>
		//         server is either REMOTE or LOCAL
		//         threads is the number to spawn
		//
		int arg = 0;
		while (arg < args.length) {
			if ( "-s".contentEquals(args[arg])) {
				String server = args[++arg];
				if ( "LOCAL".equalsIgnoreCase(server)) {
					Folders.serverURL = Folders.LOCAL_SERVER;
				}
			} else if ("-n".contentEquals(args[arg])) {
				threadCount = Integer.parseInt(args[++arg]);
			}
			arg++;
		}

		//
		//  Initialize and start client threads
		//
		for ( int i = 0; i < threadCount; i++ ) {
			try {
				LuminatiClient lc = new LuminatiClient();
				lc.setProxyPort(24000+i);
				threads.add(lc);
				try { Thread.sleep(2000 + r.nextInt(5000)); } catch ( Exception e ) {}
				lc.start();
			}
			catch ( Exception e ) {
				System.out.println("***EXCEPTION*** starting thread " + i);
			}
		}
	}

}
