package com.synesis.client.pcrb;

import com.synesis.model.EmployerKey;

public class ServerResponse {
	private EmployerKey key;
	private Integer delay;
	private String myIp;	//  This is the remote IP address of the client received by the server and handed back.

	public String getMyIp() {
		return myIp;
	}
	public void setMyIp(String myIp) {
		this.myIp = myIp;
	}
	
	public EmployerKey getKey() {
		return key;
	}
	public void setKey(EmployerKey key) {
		this.key = key;
	}
	public Integer getDelay() {
		return delay;
	}
	public void setDelay(Integer delay) {
		this.delay = delay;
	}
}
