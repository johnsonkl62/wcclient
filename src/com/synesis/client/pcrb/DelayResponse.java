package com.synesis.client.pcrb;

import java.time.LocalDateTime;

public class DelayResponse {
	private Integer delay;
	private String myIp;	//  This is the remote IP address of the client received by the server and handed back.
	private LocalDateTime ldt;

	public String getMyIp() {
		return myIp;
	}
	public void setMyIp(String myIp) {
		this.myIp = myIp;
	}
	
	public Integer getDelay() {
		return delay;
	}
	public void setDelay(Integer delay) {
		this.delay = delay;
	}

	public LocalDateTime getLdt() {
		return ldt;
	}

	public void setLdt(LocalDateTime ldt) {
		this.ldt = ldt;
	}
}
