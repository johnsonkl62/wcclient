package com.synesis.client.pcrb;

public class LocalEmployer {

	private String name;
	private Integer fn;
	private String carrier;

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Integer getFn() {
		return fn;
	}
	
	public void setFn(Integer fn) {
		this.fn = fn;
	}
	
	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public String toString() {
		return "Raw Employer:    Name = " + name + "   Fn = " + fn + "   Carrier = " + carrier;
	}
}
