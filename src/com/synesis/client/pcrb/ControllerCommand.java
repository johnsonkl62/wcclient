package com.synesis.client.pcrb;

import com.synesis.model.EmployerKey;

public class ControllerCommand {

	private String command;       //  "delay", "get", "detail"

	//  "Delay" command fields
	private Integer delay;		//  Returns "delay" status
	private String ip;  		//  "IP" address as it appears to the server
	
	//  "Get" command fields
	private EmployerKey key;	//  Returns HTML of retrieved page

	//  "Get" command fields
	private String link;	    //  Returns HTML of detail page

	public EmployerKey getKey() {
		return key;
	}
	public void setKey(EmployerKey key) {
		this.key = key;
	}
	public Integer getDelay() {
		return delay;
	}
	public void setDelay(Integer delay) {
		this.delay = delay;
	}
	public String getOption() {
		return command;
	}
	public void setOption(String option) {
		this.command = option;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
}
