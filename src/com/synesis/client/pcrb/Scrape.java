package com.synesis.client.pcrb;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


public class Scrape {

	public static void main(String[] args) {
		try {
			// Load directory
			File detailsDirectory = new File("C:\\Users\\Administrator\\Downloads");
			File[] detailsFiles = detailsDirectory.listFiles();

			FileOutputStream fos = new FileOutputStream("C:\\Users\\Administrator\\Downloads\\pcrb-details.zip");
			ZipOutputStream zos = new ZipOutputStream(fos);

			if (detailsFiles != null) {
				byte[] buffer = new byte[1024];

				for (File detailsFile : detailsFiles) {
					FileInputStream fis = new FileInputStream(detailsFile);

					// begin writing a new ZIP entry, positions the
					// stream to the start of the entry data
					zos.putNextEntry(new ZipEntry(detailsFile.getName()));

					int length;
					while ((length = fis.read(buffer)) > 0) {
						zos.write(buffer, 0, length);
					}
					zos.closeEntry();

					// close the InputStream
					fis.close();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
