package com.synesis.client.pcrb;


import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utilities {

	//
	// Find a string within another string using a regex
	//
	static public String findString(String html, String regex, String name)
	{
		String result = "";
		Pattern pattern = Pattern.compile(regex);
		Matcher m = pattern.matcher(html);
		if (m.find())
		{
			result = m.group(name);
		}
		return (result);
	}


	//
	// Find a string within another string using a regex
	//
	static public String[] findStrings(String html, String regex, String name)
	{
		ArrayList<String> results = new ArrayList<String>();
		String[] values = new String[1];
		
		Pattern pattern = Pattern.compile(regex);
		Matcher m = pattern.matcher(html);
		while (m.find())
		{
			results.add(m.group(name));
		}
		return results.toArray(values);
	}

	
	public static void main(String[] args) {
		String input = "<table id=\"dxrpContent_ContentPlaceHolder1_dxRoundPanel_dxgluCarrierName_DDD_gv_DXMainTable\" class=\"dxgvTable\" cellspacing=\"0\" cellpadding=\"0\" onclick=\"aspxGVTableClick(&#39;dxrpContent_ContentPlaceHolder1_dxRoundPanel_dxgluCarrierName_DDD_gv&#39;, event);\" style=\"width:100%;border-collapse:collapse;empty-cells:show;table-layout:fixed;overflow:hidden;\">																<tr>																	<td style=\"width:320px;\"></td><td style=\"width:65px;\"></td>																</tr><tr id=\"dxrpContent_ContentPlaceHolder1_dxRoundPanel_dxgluCarrierName_DDD_gv_DXDataRow0\" class=\"dxgvDataRow\">																	<td class=\"dxgv\" align=\"left\">-- All --</td><td class=\"dxgv\" align=\"center\">-- All --</td>																</tr><tr id=\"dxrpContent_ContentPlaceHolder1_dxRoundPanel_dxgluCarrierName_DDD_gv_DXDataRow1\" class=\"dxgvDataRow\">																	<td class=\"dxgv\" align=\"left\">21ST CENTURY CENTENNIAL INSURANCE CO</td><td class=\"dxgv\" align=\"center\">34789</td>																</tr><tr id=\"dxrpContent_ContentPlaceHolder1_dxRoundPanel_dxgluCarrierName_DDD_gv_DXDataRow2\" class=\"dxgvDataRow\">																	<td class=\"dxgv\" align=\"left\">21ST CENTURY NATIONAL INSURANCE CO</td><td class=\"dxgv\" align=\"center\">36587</td>																</tr><tr id=\"dxrpContent_ContentPlaceHolder1_dxRoundPanel_dxgluCarrierName_DDD_gv_DXDataRow3\" class=\"dxgvDataRow\">																	<td class=\"dxgv\" align=\"left\">21ST CENTURY NORTH AMERICA INSURANCE CO</td><td class=\"dxgv\" align=\"center\">32220</td>																</tr><tr id=\"dxrpContent_ContentPlaceHolder1_dxRoundPanel_dxgluCarrierName_DDD_gv_DXDataRow4\" class=\"dxgvDataRow\">																	<td class=\"dxgv\" align=\"left\">21ST CENTURY SECURITY INSURANCE CO</td><td class=\"dxgv\" align=\"center\">23833</td>																</tr><tr id=\"dxrpContent_ContentPlaceHolder1_dxRoundPanel_dxgluCarrierName_DDD_gv_DXDataRow5\" class=\"dxgvDataRow\">																	<td class=\"dxgv\" align=\"left\">ACADIA INSURANCE CO</td><td class=\"dxgv\" align=\"center\">31325</td>																</tr><tr id=\"dxrpContent_ContentPlaceHolder1_dxRoundPanel_dxgluCarrierName_DDD_gv_DXDataRow6\" class=\"dxgvDataRow\">																	<td class=\"dxgv\" align=\"left\">ACCEPTANCE INDEMNITY INSURANCE CO</td><td class=\"dxgv\" align=\"center\">20010</td>																</tr>";
		
		String[] results = Utilities.findStrings(input,"<tr id=\"dxrpContent_ContentPlaceHolder1_dxRoundPanel_dxgluCarrierName_DDD_gv_DXDataRow(?<rownum>.*?)\" class=\"dxgvDataRow\">\\s+<td class=\"dxgv\" align=\"left\">(?<carrier>.*?)</td><td class=\"dxgv\" align=\"center\">(?<naic>.*?)</td>\\s+</tr>", "carrier");
		for ( String result : results ) {
			System.out.println(result);
		}
		
	}
	
}
