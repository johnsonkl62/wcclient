package com.synesis.client.pcrb;

import com.synesis.model.Employer;
import com.synesis.model.EmployerKey;

public class ClientResponse {

//	private String command;		// Responding to this command
//	private Integer delay;		// "Delay"ed by this amount

	private Employer emp;		// "Retrieve"d this com.synesis.model
//	private EmployerKey key;		// "Retrieve"d this com.synesis.model
//	private String html;		// HTML page for com.synesis.model
//
//    private String link;		// "Detail" for this link
//    
//	public void setCommand(String command) {
//		this.command = command;
//	}
//
//	public void setDelay(Integer delay) {
//		this.delay = delay;
//	}
//
//	public void setHtml(String html) {
//		this.html = html;
//	}
//
//    public void setLink(String link) {
//		this.link = link;
//	}

	public Employer getEmp() {
		return emp;
	}

	public void setEmp(Employer emp) {
		this.emp = emp;
	}

//	public EmployerKey getKey() {
//		return key;
//	}
//
//	public void setKey(EmployerKey key) {
//		this.key = key;
//	}
}
