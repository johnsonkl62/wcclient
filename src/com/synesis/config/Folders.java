package com.synesis.config;

public class Folders {

	public static final String LOCAL_SERVER = "http://localhost:8080/wccoordinator/Luminati";
	public static final String REMOTE_SERVER = "https://kswcrc.com/wccoordinator/Luminati";
	public static String serverURL = REMOTE_SERVER ;

	//  Unix/Linux paths
	public static final String UNIX_BASE_DIRECTORY = "/synesis/";
    public static final String UNIX_COLLECTION_DIRECTORY = "/synesis/collection/";
    public static final String UNIX_PHANTOMJS_EXECUTABLE_PATH = "/usr/local/bin/phantomjs";
    public static final String UNIX_PHANTOMJS_LOGFILE_PATH = "/synesis/collection/";

    //  Windows paths
	public static final String WINDOWS_BASE_DIRECTORY = "D:\\Synesis\\";
    public static final String WINDOWS_COLLECTION_DIRECTORY = "D:\\Synesis\\Collection\\";
    public static final String WINDOWS_PHANTOMJS_EXECUTABLE_PATH = "C:\\phantomjs-2.1.1\\bin\\phantomjs.exe";
    public static final String WINDOWS_PHANTOMJS_LOGFILE_PATH = "D:\\Synesis\\Collection\\";

    //  Operational paths - default to Unix
	public static String BASE_DIRECTORY = UNIX_BASE_DIRECTORY;
    public static String COLLECTION_DIRECTORY = UNIX_COLLECTION_DIRECTORY;
    public static String PHANTOMJS_EXECUTABLE_PATH = UNIX_PHANTOMJS_EXECUTABLE_PATH;
    public static String PHANTOMJS_LOGFILE_PATH = UNIX_PHANTOMJS_LOGFILE_PATH;
   
}
